﻿using UnityEngine;
using System.Collections;

public class BaseObject : MonoBehaviour
{
    [SerializeField]
    protected GameObject m_deathExplosion;
    protected GameObject m_object;

    public int m_health;

    protected bool m_isAirUnit;
    protected bool m_isLockedOn;
    protected bool m_isTracked;

    // Use this for initialization
    void Start ()
    {
        Initialization();
        SetExplodeEffect();
    }

    public virtual void Initialization()
    {
        m_object = this.gameObject;
        m_object.layer = LayerMask.NameToLayer("Ground Enemy");
        m_object.tag = "Enemy";
        m_health = 100;
        m_isAirUnit = false;
        m_isLockedOn = false;
        m_isTracked = false;
    }

    protected virtual void SetExplodeEffect()
    {
        m_deathExplosion = Instantiate(m_deathExplosion) as GameObject;
        m_deathExplosion.SetActive(false);
    }

    // Update is called once per frame
    void Update ()
    {
        Death();
    }

    protected virtual void Death()
    {
        if (m_health <= 0)
        {
            m_object.layer = LayerMask.NameToLayer("Dead");
            m_object.SetActive(false);

            if (m_deathExplosion)
            {
                m_deathExplosion.gameObject.transform.position = m_object.transform.position;
                m_deathExplosion.SetActive(true);
            }
        }
    }

    public virtual void Damage(int m_damage)
    {
        if (m_health > 0)
        {
            m_health -= m_damage;
        }
    }

    void OnCollisionEnter(Collision m_collision)
    {
        if (m_isAirUnit == true)
        {
            if (m_collision.gameObject.tag != "Projectile")
            {
                m_health = 0;
            }
        }
    }

    public int Health
    {
        get
        {
            return m_health;
        }
    }

    public bool IsLockedOn
    {
        get
        {
            return m_isLockedOn;
        }
        set
        {
            m_isLockedOn = value;
        }
    }

    public bool IsTracked
    {
        get
        {
            return m_isTracked;
        }
        set
        {
            m_isTracked = value;
        }
    }
}
