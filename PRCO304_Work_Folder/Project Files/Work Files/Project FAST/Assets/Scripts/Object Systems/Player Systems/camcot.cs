﻿using UnityEngine;
using System.Collections;

public class camcot : MonoBehaviour
{
    protected float m_rotationSensitivity;
    protected float m_angleX;
    protected float m_currentAngleX;
    protected float m_vX;
    protected float m_xClamp;

    protected float m_angleY;
    protected float m_currentAngleY;
    protected float m_vY;
    protected float m_yClamp;

    protected float m_rotationSmooth;


    protected GameObject m_camera;
    protected Transform m_cameraTransform;

    // Use this for initialization
    void Start()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_camera = this.gameObject;
        m_cameraTransform = m_camera.GetComponent<Transform>();

        m_rotationSensitivity = 5.0f;
        m_rotationSmooth = 0.1f;
        m_vX = 0.0f;
        m_xClamp = 90.0f;
        m_vY = 0.0f;
        m_yClamp = 120.0f;
    }

    // Update is called once per frame
    void Update()
    {
        CameraRotation();
    }

    protected virtual void CameraRotation()
    {
        if ((Input.GetAxis("Cam X") < 0.0 || Input.GetAxis("Cam X") > 0.0) || (Input.GetAxis("Cam Y") < 0.0 || Input.GetAxis("Cam Y") > 0.0))
        {

            m_angleX += Input.GetAxis("Cam X") * m_rotationSensitivity;
            m_angleY += Input.GetAxis("Cam Y") * m_rotationSensitivity;

            m_angleX = Mathf.Clamp(m_angleX, -m_xClamp, 10);
            m_angleY = Mathf.Clamp(m_angleY, -m_yClamp, m_yClamp);

            //m_currentAngleX = Mathf.SmoothDamp(m_currentAngleX, m_angleX, ref m_vX, m_rotationSmooth);
            //m_currentAngleY = Mathf.SmoothDamp(m_currentAngleY, m_angleY, ref m_vY, m_rotationSmooth);

            m_cameraTransform.localRotation = Quaternion.Euler(m_angleX, m_angleY, 0);
        }
        //else
        //{
        //    m_angleX = 0;
        //    m_angleY = 0;
        //    m_cameraTransform.localRotation = Quaternion.Euler(Vector3.zero);
        //}

    }
}
