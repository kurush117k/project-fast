﻿using UnityEngine;
using System.Collections;

public class Player : BaseObject
{
    protected int m_missileAmmo;
    protected int m_spAmmo;

    protected float m_playerAltitude;
    protected float m_playerXRot;
    protected float m_playerZRot;

    protected float m_playerHeading;

    protected float m_timeOutOfArea;
    protected float m_timeOutOfAreaLimit;

    protected bool m_terrainWarning;
    protected bool m_outOfBounds;
    protected bool m_gameOver;

    public override void Initialization()
    {
        m_object = this.gameObject;
        m_object.layer = LayerMask.NameToLayer("Player");
        m_object.tag = "Player";

        m_health = 100;
        m_missileAmmo = 50;
        m_spAmmo = 10;

        m_timeOutOfArea = 0.0f;
        m_timeOutOfAreaLimit = 30.0f;

        m_isAirUnit = true;

        m_terrainWarning = false;
        m_outOfBounds = false;
        m_gameOver = false;

        m_isLockedOn = false;
        m_isTracked = false;
    }

    // Update is called once per frame
    void Update()
    {
        GameOver();
        Death();
        CalculateAltitude();
        CalculateTerrainWarning();
        GetOrientation();
        GetHeading();
    }

    protected virtual void GameOver()
    {
        if (m_timeOutOfArea >= m_timeOutOfAreaLimit)
        {
            m_gameOver = true;
        }

        if (m_health <= 0)
        {
            m_gameOver = true;
        }
    }

    protected virtual void CalculateAltitude()
    {
        Ray m_altitudeRay;
        RaycastHit m_hit;
        LayerMask m_mask;

        float m_maxAltitude;

        m_altitudeRay = new Ray(m_object.transform.position, -Vector3.up);
        m_mask = 1 << 31;
        m_maxAltitude = 15000.0f;

        if (Physics.Raycast(m_altitudeRay, out m_hit, m_maxAltitude, m_mask))
        {
            m_playerAltitude = m_hit.distance;
        }
    }

    protected virtual void CalculateTerrainWarning()
    {
        Ray m_terrainRay;
        LayerMask m_mask;

        float m_terrainWarningThreshold;

        m_terrainRay = new Ray(m_object.transform.position, -Vector3.up);
        m_mask = 1 << 29;
        m_terrainWarningThreshold = 50;

        if (Physics.Raycast(m_terrainRay, m_terrainWarningThreshold, m_mask))
        {
            m_terrainWarning = true;
        }
        else
        {
            m_terrainWarning = false;
        }
    }

    protected virtual void GetOrientation()
    {
        m_playerXRot = m_object.transform.rotation.eulerAngles.x;
        m_playerZRot = m_object.transform.rotation.eulerAngles.z;
    }

    protected virtual void GetHeading()
    {
        m_playerHeading = m_object.transform.rotation.eulerAngles.y;
    }

    public int MissileAmmo
    {
        get
        {
            return m_missileAmmo;
        }
        set
        {
            m_missileAmmo = value;
        }
    }

    public int SPAmmo
    {
        get
        {
            return m_spAmmo;
        }
        set
        {
            m_spAmmo = value;
        }
    }

    public float PlayerAltitude
    {
        get
        {
            return m_playerAltitude;
        }
    }

    public float XRot
    {
        get
        {
            return m_playerXRot;
        }
    }

    public float ZRot
    {
        get
        {
            return m_playerZRot;
        }
    }

    public float PlayerHeading
    {
        get
        {
            return m_playerHeading;
        }
    }

    public float TimeOutOfArea
    {
        get
        {
            return m_timeOutOfArea;
        }
        set
        {
            m_timeOutOfArea = value;
        }
    }

    public float TimeOutOfAreaLimit
    {
        get
        {
            return m_timeOutOfAreaLimit;
        }
    }

    public bool TerrainWarning
    {
        get
        {
            return m_terrainWarning;
        }
    }

    public bool OutOfBounds
    {
        get
        {
            return m_outOfBounds;
        }
        set
        {
            m_outOfBounds = value;
        }
    }

    public bool IsGameOver
    {
        get
        {
            return m_gameOver;
        }
    }
}
