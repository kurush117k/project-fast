﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    protected GameObject m_playerObject;

    protected Player m_player;
    protected BaseFlight m_playerFlight;
    protected F15RadarLockOn m_missileLockOn;
    protected AGMGroundLockOn m_amg65LockOn;

    protected BaseAutocannon m_autoCannon;
    protected AIM120Launcher[] m_missileLauncher;
    protected AMG65Launcher[] m_amg65Launcher;

    protected float m_analogYSensitivity;
    protected float m_analogXSensitivity;
    protected bool m_spOn;
    protected bool m_targetPaint;

	// Use this for initialization
	void Start ()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_playerObject = this.gameObject;

        m_player = m_playerObject.GetComponent<Player>();
        m_playerFlight = m_playerObject.GetComponent<BaseFlight>();
        m_missileLockOn = m_player.GetComponentInChildren<F15RadarLockOn>();
        m_amg65LockOn = m_player.GetComponentInChildren<AGMGroundLockOn>();

        m_autoCannon = m_playerObject.GetComponentInChildren<BaseAutocannon>();
        m_missileLauncher = m_playerObject.GetComponentsInChildren<AIM120Launcher>();
        m_amg65Launcher = m_playerObject.GetComponentsInChildren<AMG65Launcher>();

        //m_rocketPod = m_playerObject.GetComponentsInChildren<BaseRocketPod>();
        //m_bombDrop = m_playerObject.GetComponentsInChildren<BombDrop>();

        m_analogYSensitivity = 0.55f;
        m_analogXSensitivity = 0.65f;
        m_spOn = false;
        m_targetPaint = false;
    }

    // Update is called once per frame
    void Update ()
    {
        if (Time.timeScale != 0)
        {
            TargetSwitch();
            WeponSwitch();
            FlightControls();
            WeaponControls();
        }
        //Debug.Log(m_player.SPAmmo + " " + m_player.MissileAmmo + " " + m_spOn);
    }

    protected virtual void TargetSwitch()
    {
        if (m_spOn == false)
        {
            if (Input.GetButtonDown("Switch Target"))
            {
                m_missileLockOn.SelectTarget();
            }

            if (Input.GetButton("Target Paint"))
            {
                m_targetPaint = true;
            }
            else
            {
                m_targetPaint = false;
            }
        }
        else if (m_spOn == true)
        {
            if (Input.GetButtonDown("Switch Target"))
            {
                m_amg65LockOn.SelectTarget();
            }

            if (Input.GetButton("Target Paint"))
            {
                m_targetPaint = true;
            }
            else
            {
                m_targetPaint = false;
            }
        }
    }

    protected virtual void WeponSwitch()
    {
        if (Input.GetButtonDown("Switch Weapon"))
        {
            if (m_spOn == false)
            {
                m_spOn = true;
                m_missileLockOn.enabled = false;
            }
            else if (m_spOn == true)
            {
                m_spOn = false;
                m_missileLockOn.enabled = true;
            }
        }
    }

    protected virtual void FlightControls()
    {
        //ROll
        m_playerFlight.Roll(-Input.GetAxis("Roll") * m_analogXSensitivity);
        //PITCH
        m_playerFlight.Pitch(-Input.GetAxis("Pitch") * m_analogYSensitivity);
        //YAW
        if (Input.GetButton("Yaw Left"))
        {
            m_playerFlight.Yaw(-m_analogYSensitivity);
        }
        else if (Input.GetButton("Yaw Right"))
        {
            m_playerFlight.Yaw(m_analogYSensitivity);
        }
        //Decelerate
        if (Input.GetAxis("Decelerate") > 0.0f)
        {
            m_playerFlight.Decelerate(Input.GetAxis("Decelerate"));
        }
        //Accelerate
        else if (Input.GetAxis("Accelerate") > 0.0f)
        {
            m_playerFlight.Accelerate(Input.GetAxis("Accelerate"));
        }
        //Defult Speed
        else
        {
            m_playerFlight.DefaultSpeed();
        }
    }

    protected virtual void WeaponControls()
    {
        if (Input.GetButton("Launch"))
        {
            m_autoCannon.AutocannonFire();
        }

        if (Input.GetButtonDown("Fire"))
        {
            if (m_spOn == false)
            {
                if (m_player.MissileAmmo > 0)
                {
                    for (int n = 0; n < m_missileLauncher.Length; n++)
                    {
                        if (m_missileLauncher[n].Armed == true)
                        {
                            m_missileLauncher[n].MissileLaunch();
                            m_player.MissileAmmo -= 1;
                            break;
                        }
                    }
                }
            }

            if (m_spOn == true)
            {
                if (m_player.SPAmmo > 0)
                {
                    for (int n = 0; n < m_amg65Launcher.Length; n++) 
                    {
                        if (m_amg65Launcher [n].Armed == true)
                        {
                            m_amg65Launcher[n].MissileLaunch();
                            m_player.SPAmmo -= 1;
                            break;
                        }
                    }
                }
            }
        }
    }

    public bool SPOn
    {
        get
        {
            return m_spOn;
        }
    }

    public bool TargetPaint
    {
        get
        {
            return m_targetPaint;
        }
    }


}
