﻿using UnityEngine;
using System.Collections;

public class F15PlayerSytem : Player
{
    public override void Initialization()
    {
        m_object = this.gameObject;
        m_object.layer = LayerMask.NameToLayer("Player");
        m_object.tag = "Player";

        m_health = 120;
        m_missileAmmo = 60;
        m_spAmmo = 20;

        m_timeOutOfArea = 0.0f;
        m_timeOutOfAreaLimit = 30.0f;

        m_isAirUnit = true;

        m_terrainWarning = false;
        m_outOfBounds = false;
        m_gameOver = false;

        m_isLockedOn = false;
        m_isTracked = false;
    }
}
