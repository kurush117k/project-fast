﻿using UnityEngine;
using System.Collections;

public class SU35Object : JetAIObject
{
    public override void Initialization()
    {
        m_object = this.gameObject;
        m_object.layer = LayerMask.NameToLayer("Air Enemy");
        m_object.tag = "Enemy";
        m_health = 100;
        m_isAirUnit = true;
    }
}
