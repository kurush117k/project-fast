﻿using UnityEngine;
using System.Collections;

public class JetAIObject : BaseObject 
{
    public override void Initialization()
    {
        m_object = this.gameObject;
        m_object.layer = LayerMask.NameToLayer("Enemy");
        m_object.tag = "Enemy";
        m_health = 50;
        m_isAirUnit = true;
    }
}
