﻿using UnityEngine;
using System.Collections;

public class SAMObject : BaseObject
{
    public override void Initialization()
    {
        m_object = this.gameObject;
        m_object.layer = LayerMask.NameToLayer("Ground Enemy");
        m_object.tag = "Enemy";
        m_health = 50;
        m_isAirUnit = false;
        m_isLockedOn = false;
        m_isTracked = false;
    }
}
