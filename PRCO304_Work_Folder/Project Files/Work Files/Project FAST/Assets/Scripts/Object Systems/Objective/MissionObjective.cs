﻿using UnityEngine;
using System.Collections;

public class MissionObjective : BaseObject
{
    protected bool m_isCounted;

    public override void Initialization()
    {
        m_object = this.gameObject;
        m_object.layer = LayerMask.NameToLayer("Ground Enemy");
        m_object.tag = "Enemy";
        m_health = 100;
        m_isAirUnit = false;
        m_isLockedOn = false;
        m_isTracked = false;
        m_isCounted = false;
    }

    public bool IsCounted
    {
        get
        {
            return m_isCounted;
        }
        set
        {
            m_isCounted = value;
        }
    }
}
