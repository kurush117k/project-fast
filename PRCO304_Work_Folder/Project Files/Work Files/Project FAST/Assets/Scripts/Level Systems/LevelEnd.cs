﻿using UnityEngine;
using System.Collections;

public class LevelEnd : MonoBehaviour
{
    protected GameObject m_levelEnd;
    protected GameManager m_gameManager;
    // Use this for initialization
    void Start()
    {
        Initialization();
    }

    protected virtual void Initialization()
    {
        m_levelEnd = this.gameObject;
        m_gameManager = m_levelEnd.GetComponentInParent<GameManager>();
    }

    void OnTriggerEnter(Collider m_collider)
    {
        if(m_collider .tag == "Player")
        {
            if (m_gameManager.ObjectiveFinished == true) 
            {
                m_gameManager.MissionFinished = true;
            }
        }
    }
}