﻿using UnityEngine;
using System.Collections;

public class LevelBoundaries : MonoBehaviour
{
    protected bool m_outOfArea;

    protected float m_time;

    protected string m_tagName;

    protected GameObject m_objectOutOfArea;

	// Use this for initialization
	void Start ()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_time = 0.0f;
        m_outOfArea = false;
        m_tagName = "Player";
    }

    // Update is called once per frame
    void Update ()
    {
        OutofArea();
    }

    protected virtual void OutofArea()
    {
        Player m_player;

        if (m_objectOutOfArea != null)
        {
            m_player = m_objectOutOfArea.GetComponent<Player>();

            if (m_outOfArea == true)
            {
                if (Time.timeScale != 0)
                {
                    m_time = m_time + Time.deltaTime;

                    m_player.TimeOutOfArea = m_time;
                    m_player.OutOfBounds = true;
                }
            }
            else if (m_outOfArea == false)
            {
                m_time = 0.0f;
                m_player.TimeOutOfArea = m_time;
                m_player.OutOfBounds = false;
            }
        }
    }

    void OnTriggerExit(Collider m_collider)
    {
        if (m_collider.tag == m_tagName) 
        {
            m_outOfArea = true;
            m_objectOutOfArea = m_collider.gameObject;
        }
    }

    void OnTriggerEnter(Collider m_collider)
    {
        if (m_collider.tag == m_tagName)
        {
            m_outOfArea = false;
            m_objectOutOfArea = m_collider.gameObject;
        }
    }

    void OnTriggerStay(Collider m_collider)
    {
        if (m_collider.tag == m_tagName)
        {
            m_outOfArea = false;
            m_objectOutOfArea = m_collider.gameObject;
        }
    }
}
