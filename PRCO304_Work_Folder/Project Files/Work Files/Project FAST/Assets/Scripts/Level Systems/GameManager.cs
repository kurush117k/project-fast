﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    protected GameObject[] m_missionObjective;
    [SerializeField]
    protected Transform m_playerSpawn;
    [SerializeField]
    protected GameObject m_levelEnd;
    [SerializeField]
    protected GameObject m_playerObject;
    protected Player m_player;

    protected bool m_gamePaused;
    protected bool m_missionObjectiveFinished;
    protected bool m_missionFinished;

    protected int m_missionObjecttiveCount;
    protected int m_missionObjecttiveTotal;

    protected string m_gameOverLevel;
    protected string m_missionFinishedLevel;

    // Use this for initialization
    void Start ()
    {
        Initialization();
        PlayerSpawn();
    }

    protected virtual void Initialization()
    {
        //m_player = m_playerObject.GetComponent<Player>();

        //m_missionObjective = FindObjectsOfType(typeof(MissionObjective)) as GameObject[];

        m_missionObjecttiveCount = 0;
        m_missionObjecttiveTotal = m_missionObjective.Length;

        m_missionObjectiveFinished = false;
        m_missionFinished = false;
        m_gamePaused = false;

        m_gameOverLevel = "game_over";
        m_missionFinishedLevel = "level_finished";
    }

    protected virtual void PlayerSpawn()
    {
        m_playerObject = Instantiate(m_playerObject);
        m_playerObject.SetActive(true);
        m_player = m_playerObject.GetComponent<Player>();
        m_player.transform.position = m_playerSpawn.position;
        m_playerObject.transform.rotation = Quaternion.Euler(m_playerSpawn.rotation.x, 45.0f, m_playerSpawn.rotation.z);
    }

    // Update is called once per frame
    void Update ()
    {
        GamePause();
        GameOver();
        MissionObjective();
        LevelFinished();
	}

    protected virtual void GamePause()
    {
        if (Input.GetButtonDown("Pause") && m_gamePaused == false)
        {
            Debug.Log("Pause");
            Time.timeScale = 0;
            m_gamePaused = true;
        }
        else if (Input.GetButtonDown("Pause") && m_gamePaused == true)
        {
            Debug.Log("Play");
            Time.timeScale = 1;
            m_gamePaused = false;
        }
    } 

    protected virtual void GameOver()
    {
        if (m_player.IsGameOver == true) 
        {
            SceneManager.LoadScene(m_gameOverLevel);
        }
    }

    protected virtual void MissionObjective()
    {
        foreach (GameObject m_objective in m_missionObjective)
        {
            if (m_objective.activeSelf == false)
            {
                MissionObjective m_missionObj;

                m_missionObj = m_objective.GetComponent<MissionObjective>();
                if (m_missionObj.IsCounted == false)
                {
                    m_missionObjecttiveCount++;
                    m_missionObj.IsCounted = true;
                }
            }
        }

        if (m_missionObjecttiveCount >= m_missionObjecttiveTotal)
        {
            m_missionObjectiveFinished = true;
        }
    }

    protected virtual void LevelFinished()
    {
        if (m_missionFinished == true)
        {
            SceneManager.LoadScene(m_missionFinishedLevel);
        }
    }

    public bool ObjectiveFinished
    {
        get
        {
            return m_missionObjectiveFinished;
        }
    }

    public bool MissionFinished
    {
        get
        {
            return m_missionFinished;
        }
        set
        {
            m_missionFinished = value;
        }
    }
}
