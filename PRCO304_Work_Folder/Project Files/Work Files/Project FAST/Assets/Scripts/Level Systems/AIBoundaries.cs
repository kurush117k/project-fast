﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIBoundaries : MonoBehaviour
{
    protected string m_tagName;

    protected List<GameObject> m_aiObjects;
    protected List<GameObject> m_aiOutOfBounds;

    // Use this for initialization
    void Start ()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_aiObjects = new List<GameObject>();
        m_aiOutOfBounds = new List<GameObject>();
    }

    // Update is called once per frame
    void Update ()
    {
        BoundAI();
	}

    protected virtual void BoundAI()
    {
        foreach (GameObject m_ai in m_aiOutOfBounds)
        {
            BaseAirSense m_aiSense;

            m_aiSense = m_ai.GetComponent<BaseAirSense>();
            m_aiSense.OutOfBounds = true;
        }

        foreach (GameObject m_ai in m_aiObjects)
        {
            BaseAirSense m_aiSense;

            m_aiSense = m_ai.GetComponent<BaseAirSense>();
            m_aiSense.OutOfBounds = false;
        }
    }

    void OnTriggerExit(Collider m_collider)
    {
        if (m_collider.tag == m_tagName)
        {
            m_aiObjects.Remove(m_collider.gameObject);
            m_aiOutOfBounds.Add(m_collider.gameObject);
        }
    }

    void OnTriggerEnter(Collider m_collider)
    {
        if (m_collider.tag == m_tagName)
        {
            m_aiObjects.Add(m_collider.gameObject);
            m_aiOutOfBounds.Remove(m_collider.gameObject);
        }
    }
}
