﻿using UnityEngine;
using System.Collections;

public class Explode : MonoBehaviour
{
    protected GameObject m_explodeObject;
    protected ParticleSystem m_explode;

	// Use this for initialization
	void Start ()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_explodeObject = this.gameObject;
        m_explode = m_explodeObject.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update ()
    {
        ExplodeEffect();
    }

    protected virtual void ExplodeEffect()
    {
        if (m_explode.isPlaying == false)
        {
            m_explode.Play();
            StartCoroutine(EffectSetActive(m_explode.duration));
        }
    }

    protected IEnumerator EffectSetActive(float m_waitTime)
    {
        yield return new WaitForSeconds(m_waitTime);

        this.gameObject.SetActive(false);
    }
}
