﻿using UnityEngine;
using System.Collections;

public class HUDManager : MonoBehaviour
{
    public GameObject m_health;
    public GameObject m_missileAmmo;
    public GameObject m_spAmmo;
    public GameObject m_altitude;
    public GameObject m_velocity;
    public GameObject m_terrainWarning;
    public GameObject m_missileWarning;
    public GameObject m_lockOn;
    public GameObject m_spLockOn;
    public GameObject m_heat;
    public GameObject m_outOfBounds;
    public GameObject m_cannonAmmo;
    public GameObject m_cannonTarget;
    public GameObject m_hudRot;
    public GameObject m_hudHeading;
    public GameObject m_hudAnchor;
    public GameObject m_spBack;
    public GameObject m_msslBack;
    public GameObject m_aimPaint;
    public GameObject m_missileReady;
    public GameObject m_damageIndicator;

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
