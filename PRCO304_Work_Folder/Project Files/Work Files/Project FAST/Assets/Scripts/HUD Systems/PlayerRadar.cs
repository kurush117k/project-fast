﻿using UnityEngine;
using System.Collections;

public class PlayerRadar : RadarMap
{

    protected override void Initialization()
    {

    }

    // Update is called once per frame
    void Update ()
    {
        m_radarLocation = m_radarLocationValues.bottomLeft.ToString();
        SetRadarLocation();
        m_radarScale = 0.0108125f;
        m_radarMapPercent = 15.0f;

        m_radarCenter = this.gameObject.GetComponent<Transform>();
    }
}
