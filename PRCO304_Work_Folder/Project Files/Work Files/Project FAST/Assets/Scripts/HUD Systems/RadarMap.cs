﻿using UnityEngine;
using System.Collections;

public class RadarMap : MonoBehaviour
{
    [SerializeField]
    protected Texture m_playerBlip;
    [SerializeField]
    protected Texture m_enemyBlip;
    [SerializeField]
    protected Texture m_radarBackground;

    protected Transform m_radarCenter;
    protected Vector2 m_radarMapCenter;
    protected Vector2 m_radarMapCenterCustom;

    protected float m_radarScale;
    protected float m_radarMapPercent;
    protected float m_radarWidth;
    protected float m_radarHight;
    protected string m_radarLocation;
    // Use this for initialization

    void Start ()
    {
        Initialization();
    }

    protected virtual void Initialization()
    {
        m_radarLocation = m_radarLocationValues.bottomLeft.ToString();
        SetRadarLocation();
        m_radarScale = 0.01075f;
        m_radarMapPercent = 15.0f;

        m_radarCenter = this.gameObject.GetComponent<Transform>();
    }

    protected virtual void SetRadarLocation()
    {
        //m_radarWidth = Screen.width * m_radarMapPercent / 100;
        //m_radarHight = m_radarWidth;

        m_radarWidth = 200.0f;
        m_radarHight = 200.0f;

        if (m_radarLocation == m_radarLocationValues.topLeft.ToString())
        {
            m_radarMapCenter = new Vector2(m_radarWidth / 2, m_radarHight / 2);
        }
        if (m_radarLocation == m_radarLocationValues.topCenter.ToString())
        {
            m_radarMapCenter = new Vector2(Screen.width / 2, m_radarHight / 2);
        }
        if (m_radarLocation == m_radarLocationValues.topRight.ToString())
        {
            m_radarMapCenter = new Vector2(Screen.width - m_radarWidth / 2, m_radarHight / 2);
        }
        if (m_radarLocation == m_radarLocationValues.middleLeft.ToString())
        {
            m_radarMapCenter = new Vector2(m_radarWidth / 2, Screen.height / 2);
        }
        if (m_radarLocation == m_radarLocationValues.middleCenter.ToString())
        {
            m_radarMapCenter = new Vector2(Screen.width / 2, Screen.height / 2);
        }
        if (m_radarLocation == m_radarLocationValues.middleRight.ToString())
        {
            m_radarMapCenter = new Vector2(Screen.width - m_radarWidth / 2, Screen.height / 2);
        }
        if (m_radarLocation == m_radarLocationValues.bottomLeft.ToString())
        {
            m_radarMapCenter = new Vector2(m_radarWidth / 2 + 100, Screen.height - m_radarHight / 2 - 100);
        }
        if (m_radarLocation == m_radarLocationValues.bottomCenter.ToString())
        {
            m_radarMapCenter = new Vector2(Screen.width / 2, Screen.height - m_radarHight / 2);
        }
        if (m_radarLocation == m_radarLocationValues.bottomRight.ToString())
        {
            m_radarMapCenter = new Vector2(Screen.width - m_radarWidth / 2, Screen.height - m_radarHight / 2);
        }
        if (m_radarLocation == m_radarLocationValues.custom.ToString())
        {
            m_radarMapCenter = m_radarMapCenterCustom;
        }
    }

    // Update is called once per frame
    void Update ()
    {
	
	}

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(m_radarMapCenter.x - m_radarWidth / 2, m_radarMapCenter.y - m_radarHight / 2, m_radarWidth, m_radarHight), m_radarBackground);

        DrawEnemyBlip();
        DrawPlayerBlip();
    }

    protected void DrawPlayerBlip()
    {
        GameObject[] m_gos;

        m_gos = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject m_go in m_gos)
        {
            DrawBlip(m_go, m_playerBlip);
        }
    }

    protected void DrawEnemyBlip()
    {
        GameObject[] m_gos;

        m_gos = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject m_go in m_gos)
        {
            DrawBlip(m_go, m_enemyBlip);
        }
    }

    protected void DrawBlip(GameObject m_go, Texture m_texture)
    {
        Vector3 m_centPos;
        Vector3 m_extPos;

        float m_dist;
        float m_distX;
        float m_distZ;
        float m_angX;
        float m_angY;
        float m_deltaY;

        m_centPos = m_radarCenter.position;
        m_extPos = m_go.transform.position;

        m_dist = Vector3.Distance(m_centPos, new Vector3(m_extPos.x, m_centPos.y, m_extPos.z));
      
        m_distX = m_centPos.x - m_extPos.x;
        m_distZ = m_centPos.z - m_extPos.z;

        m_deltaY = Mathf.Atan2(m_distX, m_distZ) * Mathf.Rad2Deg - 270 - m_radarCenter.eulerAngles.y;

        m_angX = m_dist * Mathf.Cos(m_deltaY * Mathf.Deg2Rad);
        m_angY = m_dist * Mathf.Sin(m_deltaY * Mathf.Deg2Rad);

        m_angX = m_angX * m_radarScale;
        m_angY = m_angY * m_radarScale;

        if (m_dist <= 16000)
        {
            GUI.DrawTexture(new Rect(m_radarMapCenter.x + m_angX - 2, m_radarMapCenter.y + m_angY - 2, 4, 4), m_texture);
        }
    }
    protected enum m_radarLocationValues
    {
        topLeft,
        topCenter,
        topRight,
        middleLeft,
        middleCenter,
        middleRight,
        bottomLeft,
        bottomCenter,
        bottomRight,
        custom
    }
}
