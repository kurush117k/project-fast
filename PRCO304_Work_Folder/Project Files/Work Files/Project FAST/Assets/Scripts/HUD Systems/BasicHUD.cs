﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BasicHUD : MonoBehaviour
{
    protected GameObject m_playerObject;
    protected GameObject m_hud;
    protected Camera m_camera;

    protected HUDManager m_hudManager;
    protected Player m_player;
    protected PlayerController m_controller;

    protected F15RadarLockOn m_lockOn;
    protected AGMGroundLockOn m_agm65Lockon;

    protected BaseFlight m_flight;
    protected PlayerAutoCannon m_autoCannon;
    protected AIM120Launcher[] m_missileLauncher;
    protected AMG65Launcher[] m_amg65Launcher;

    protected int m_velocity;
    protected int m_altitude;
    protected int m_heading;
    protected int m_heat;

    protected int m_health;
    protected int m_maxHealth;
    protected float m_displayedHealth;

    protected int m_spAmmo;
    protected int m_missileAmmo;
    protected int m_cannonAmmo;

    protected int m_timeOutOfBounds;
    protected int m_maxTimeOutOfBounds;

    [SerializeField]
    protected Texture m_viableTarget;
    [SerializeField]
    protected Texture m_viableTGTTarget;
    [SerializeField]
    protected Texture m_noTarget;
    [SerializeField]
    protected AudioSource m_lockSound;
    [SerializeField]
    protected AudioSource m_missileWarningSound;
    [SerializeField]
    protected AudioSource m_terrainWarningSound;
    [SerializeField]
    protected Transform m_anchorPoint;
    protected RectTransform m_hudAnchor;

    protected RawImage m_msslBack;
    protected RawImage m_spBack;

    protected RawImage m_lockOnHUD;
    protected RectTransform m_lockOnRect;

    protected RawImage m_spLockOnHUD;
    protected RectTransform m_spLockOnRect;

    protected RawImage m_autoCannonTarget;
    protected RectTransform m_autoCannonTargetRect;

    protected RectTransform m_hudRot;

    protected RawImage m_damageIndicator;
    protected RawImage m_aimPaint;
    protected RawImage[] m_missileReady;

    protected Text m_healthHUD;
    protected Text m_missileAmmoHUD;
    protected Text m_spAmmoHUD;
    protected Text m_cannonAmmoHUD;
    protected Text m_heatHUD;

    protected Text m_altitudeHUD;
    protected Text m_velocityHUD;
    protected Text m_terrainWarningHUD;
    protected Text m_missileWarningHUD;
    protected Text m_headingHUD;
    protected Text m_outOfBoundsHUD;

    
   
    // Use this for initialization
    void Start()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_playerObject = this.gameObject;

        m_player = m_playerObject.GetComponent<Player>();
        m_controller = m_playerObject.GetComponent<PlayerController>();
        m_flight = m_playerObject.GetComponent<BaseFlight>();

        m_lockOn = m_playerObject.GetComponentInChildren<F15RadarLockOn>();
        m_agm65Lockon = m_playerObject.GetComponentInChildren<AGMGroundLockOn>();

        m_autoCannon = m_playerObject.GetComponentInChildren<PlayerAutoCannon>();

        m_missileLauncher = m_playerObject.GetComponentsInChildren<AIM120Launcher>();
        m_amg65Launcher = m_playerObject.GetComponentsInChildren<AMG65Launcher>();

        m_velocity = (int)m_flight.Velocity;
        m_altitude = (int)m_player.PlayerAltitude;
        m_heading = (int)m_player.PlayerHeading;

        m_health = m_player.Health;
        m_maxHealth = m_health;
        m_spAmmo = m_player.SPAmmo;
        m_missileAmmo = m_player.MissileAmmo;
        m_cannonAmmo = m_autoCannon.Ammo;
        m_heat = (int)m_autoCannon.Heat;

        m_timeOutOfBounds = (int)m_player.TimeOutOfArea;
        m_maxTimeOutOfBounds = (int)m_player.TimeOutOfAreaLimit;

        m_camera = m_playerObject.GetComponentInChildren<Camera>();

        m_hud = GameObject.Find("HUD");
        m_hudManager = m_hud.GetComponent<HUDManager>();

        m_healthHUD = m_hudManager.m_health.GetComponent<Text>();
        m_altitudeHUD = m_hudManager.m_altitude.GetComponent<Text>();
        m_velocityHUD = m_hudManager.m_velocity.GetComponent<Text>();
        m_terrainWarningHUD = m_hudManager.m_terrainWarning.GetComponent<Text>();
        m_missileWarningHUD = m_hudManager.m_missileWarning.GetComponent<Text>();
        m_outOfBoundsHUD = m_hudManager.m_outOfBounds.GetComponent<Text>();

        m_missileAmmoHUD = m_hudManager.m_missileAmmo.GetComponent<Text>();
        m_spAmmoHUD = m_hudManager.m_spAmmo.GetComponent<Text>();
        m_cannonAmmoHUD = m_hudManager.m_cannonAmmo.GetComponent<Text>();
        m_heatHUD = m_hudManager.m_heat.GetComponent<Text>();
        m_headingHUD = m_hudManager.m_hudHeading.GetComponent<Text>();

        m_msslBack = m_hudManager.m_msslBack.GetComponent<RawImage>();
        m_spBack = m_hudManager.m_spBack.GetComponent<RawImage>();

        m_hudAnchor = m_hudManager.m_hudAnchor.GetComponent<RectTransform>();

        m_lockOnHUD = m_hudManager.m_lockOn.GetComponent<RawImage>();
        m_lockOnRect = m_hudManager.m_lockOn.GetComponent<RectTransform>();

        m_spLockOnHUD = m_hudManager.m_spLockOn.GetComponent<RawImage>();
        m_spLockOnRect = m_hudManager.m_spLockOn.GetComponent<RectTransform>();

        m_damageIndicator = m_hudManager.m_damageIndicator.GetComponent<RawImage>();
        m_aimPaint = m_hudManager.m_aimPaint.GetComponent<RawImage>();
        m_missileReady = m_hudManager.m_missileReady.GetComponentsInChildren<RawImage>();

        m_autoCannonTarget = m_hudManager.m_cannonTarget.GetComponent<RawImage>();
        m_autoCannonTargetRect = m_hudManager.m_cannonTarget.GetComponent<RectTransform>();

        m_hudRot = m_hudManager.m_hudRot.GetComponent<RectTransform>();

        m_maxHealth = 120;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateValues();
        UpdateHUD();
        MissileLock();
        MissileReady();
        MissileWarning();
        AutoCannoTarget();
        TerrainWarning();
        HudRot();
        AnchorHUD();
    }

    protected virtual void UpdateValues()
    {
        int m_tempPercent;

        m_tempPercent = 100;
        m_velocity = (int)(m_flight.Velocity / 160);
        m_altitude = (int)m_player.PlayerAltitude;
        m_heading = (int)m_player.PlayerHeading;

        m_health = m_player.Health;
        m_displayedHealth = m_health * m_tempPercent / m_maxHealth;

        m_spAmmo = m_player.SPAmmo;
        m_missileAmmo = m_player.MissileAmmo;
        m_cannonAmmo = m_autoCannon.Ammo;
        m_heat = (int)m_autoCannon.Heat;

        m_timeOutOfBounds = (int)m_player.TimeOutOfArea;
        m_maxTimeOutOfBounds = (int)m_player.TimeOutOfAreaLimit;
    }

    protected virtual void UpdateHUD()
    {
        if(m_displayedHealth <=66 && m_displayedHealth > 33)
        {
            m_damageIndicator.color = Color.yellow;
        }
        else if (m_displayedHealth <=33)
        {
            m_damageIndicator.color = Color.red;
        }
        else
        {
            m_damageIndicator.color = Color.green;
        }

        if (m_displayedHealth < 100 && m_displayedHealth >= 10)
        {
            m_healthHUD.text = "-" + m_displayedHealth.ToString() + "%";
        }
        else if (m_displayedHealth < 10) 
        {
            m_healthHUD.text = "--" + m_displayedHealth.ToString() + "%";
        }
        else
        {
            m_healthHUD.text = m_displayedHealth.ToString() + "%";
        }

        m_altitudeHUD.text = m_altitude.ToString();
        m_headingHUD.text = m_heading.ToString();
        m_velocityHUD.text = m_velocity.ToString();

        if (m_controller.SPOn == false)
        {
            m_msslBack.enabled = true;
            m_spBack.enabled = false;
        }
        if (m_controller.SPOn == true)
        {
            m_msslBack.enabled = false;
            m_spBack.enabled = true;
        }

        if (m_spAmmo >= 10)
        {
            m_spAmmoHUD.text = "--" + m_spAmmo.ToString();
        }
        else if (m_spAmmo < 10)
        {
            m_spAmmoHUD.text = "---" + m_spAmmo.ToString();
        }

        if (m_missileAmmo >= 10)
        {
            m_missileAmmoHUD.text = "--" + m_missileAmmo.ToString();
        }
        else if (m_missileAmmo < 10)
        {
            m_missileAmmoHUD.text = "---" + m_missileAmmo.ToString();
        }

        if (m_cannonAmmo < 1000 && m_cannonAmmo >= 100)
        {
            m_cannonAmmoHUD.text = "-" + m_cannonAmmo.ToString();
        }
        else if (m_cannonAmmo < 100 && m_cannonAmmo >= 10)
        {
            m_cannonAmmoHUD.text = "--" + m_cannonAmmo.ToString();
        }
        else if (m_cannonAmmo < 10)
        {
            m_cannonAmmoHUD.text = "---" + m_cannonAmmo.ToString();
        }
        else
        {
            m_cannonAmmoHUD.text = m_cannonAmmo.ToString();
        }

        if (m_heat < 100 && m_heat >= 10)
        {
            m_heatHUD.text = "-" + m_heat.ToString() + "%";
        }
        else if (m_heat < 10)
        {
            m_heatHUD.text = "--" + m_heat.ToString() + "%";
        }
        else
        {
            m_heatHUD.text = m_heat.ToString() + "%";
        }

        if (m_player.OutOfBounds == true)
        {
            m_outOfBoundsHUD.text = "Out Of Bounds - Return : " + m_timeOutOfBounds.ToString() + "/" + m_maxTimeOutOfBounds.ToString();
        }
        else if (m_player.OutOfBounds == false)
        {
            m_outOfBoundsHUD.text = "";
        }
    }

    protected virtual void MissileLock()
    {
        if (m_controller.SPOn == false)
        {
            if (m_lockOn.Target != null)
            {
                Vector3 m_lockOnHudPos;

                m_lockOnHudPos = m_camera.WorldToScreenPoint(m_lockOn.Target.transform.position);
                m_lockOnRect.position = new Vector3(m_lockOnHudPos.x, m_lockOnHudPos.y, 0);

                if (m_lockOnHudPos.z <= 0)
                {
                    m_lockOnHUD.enabled = false;
                }
                else
                {
                    m_lockOnHUD.enabled = true;
                }

                if (m_lockOn.TargetLockedOn == null)
                {
                    m_lockOnHUD.color = Color.red;
                }
                else
                {
                    m_lockOnHUD.color = Color.green;
                    if (m_lockSound)
                    {
                        if(m_lockSound .isPlaying == false)
                        {
                            m_lockSound.Play();
                        }
                    }
                }
            }
            else
            {
                m_lockOnHUD.enabled = false;
            }
        }
        else
        {
            m_lockOnHUD.enabled = false;
        }

        if (m_controller.SPOn == true)
        {
            if (m_agm65Lockon.Target != null)
            {
                Vector3 m_lockOnHudPos;

                m_lockOnHudPos = m_camera.WorldToScreenPoint(m_agm65Lockon.Target.transform.position);
                m_spLockOnRect.position = new Vector3(m_lockOnHudPos.x, m_lockOnHudPos.y, 0);

                if (m_lockOnHudPos.z <= 0)
                {
                    m_spLockOnHUD.enabled = false;
                }
                else
                {
                    m_spLockOnHUD.enabled = true;
                }

                if (m_agm65Lockon.TargetLockedOn == null)
                {
                    m_spLockOnHUD.color = Color.red;
                }
                else
                {
                    m_spLockOnHUD.color = Color.green;
                    if (m_lockSound)
                    {
                        if (m_lockSound.isPlaying == false)
                        {
                            m_lockSound.Play();
                        }
                    }
                }
            }
            else
            {
                m_spLockOnHUD.enabled = false;
            }
        }
        else
        {
            m_spLockOnHUD.enabled = false;
        }

        if (m_controller.SPOn == false)
        {
            if (m_controller.TargetPaint == true)
            {
                m_aimPaint.enabled = true;
                m_lockOn.PrecisionTarget(m_camera.ScreenPointToRay(m_hudManager.m_aimPaint.transform.position));
            }
            else
            {
                m_aimPaint.enabled = false;
            }
        }
        if (m_controller.SPOn == true)
        {
            if (m_controller.TargetPaint == true)
            {
                m_aimPaint.enabled = true;
                m_agm65Lockon.PrecisionTarget(m_camera.ScreenPointToRay(m_hudManager.m_aimPaint.transform.position));
            }
            else
            {
                m_aimPaint.enabled = false;
            }
        }
    }

    protected virtual void AutoCannoTarget()
    {
        if (m_autoCannon.TargetSight == true && m_autoCannon.Hit == Vector3.zero)
        {
            m_autoCannonTarget.enabled = true;
            m_autoCannonTarget.color = Color.white;
        }
        else if (m_autoCannon.TargetSight == true && m_autoCannon.Hit != Vector3.zero)
        {
            Vector3 m_autoCannonHUDPos;
            Vector3 m_targetPos;

            m_targetPos = m_autoCannon.Hit;
            m_autoCannonHUDPos = m_camera.WorldToScreenPoint(m_targetPos);
            m_autoCannonTargetRect.position = new Vector3(m_autoCannonHUDPos.x, m_autoCannonHUDPos.y, 0);
            m_autoCannonTarget.enabled = true;
            m_autoCannonTarget.color = Color.red;
        }
        else
        {
            m_autoCannonTarget.enabled = false;
        }
    }

    protected virtual void MissileReady()
    {
        if (m_controller.SPOn == false)
        {
            for (int i = 0; i < m_missileReady.Length; i++)
            {
                if (m_missileLauncher[i].Armed == true)
                {
                    m_missileReady[i].color = Color.green;
                }
                else
                {
                    m_missileReady[i].color = Color.red;
                }
            }
        }
        else if (m_controller.SPOn == true)
        {
            for (int i = 0; i < m_missileReady.Length; i++)
            {
                if (m_amg65Launcher[i].Armed == true)
                {
                    m_missileReady[i].color = Color.green;
                }
                else
                {
                    m_missileReady[i].color = Color.red;
                }
            }
        }
    }

    protected virtual void AnchorHUD()
    {
        Vector3 m_anchorHudPos;

        m_anchorHudPos = m_camera.WorldToScreenPoint(m_anchorPoint.transform.position);

        if (m_anchorHudPos.z <= 0) 
        {
            m_hudAnchor.gameObject.SetActive(false);
        }
        else
        {
            m_hudAnchor.gameObject.SetActive(true);
            m_hudAnchor.position = new Vector3(m_anchorHudPos.x, m_anchorHudPos.y, 0);
        }
    }

    protected virtual void MissileWarning()
    {
        if (m_player.IsLockedOn == true && m_player.IsTracked == false)
        {
            m_missileWarningHUD.text = "Locked On : Evade";
        }
        else if (m_player.IsLockedOn == true && m_player.IsTracked == true)
        {
            m_missileWarningHUD.text = "Missile : Evade";
            if (m_missileWarningSound)
            {
                if (m_missileWarningSound.isPlaying == false)
                {
                    m_missileWarningSound.Play();
                }
            }
        }
        else if (m_player.IsLockedOn == false && m_player.IsTracked == true)
        {
            m_missileWarningHUD.text = "Missile : Evade";
            if (m_missileWarningSound)
            {
                if (m_missileWarningSound.isPlaying == false)
                {
                    m_missileWarningSound.Play();
                }
            }
        }
        else
        {
            m_missileWarningHUD.text = "";
        }
    }

    protected virtual void TerrainWarning()
    {
        if (m_player.TerrainWarning == true)
        {
            m_terrainWarningHUD.text = "TERRAIN : PULL UP";
            if (m_terrainWarningSound)
            {
                if (m_terrainWarningSound.isPlaying == false)
                {
                    m_terrainWarningSound.Play();
                }
            }
        }
        else
        {
            m_terrainWarningHUD.text = "";
            if (m_terrainWarningSound)
            {
                if (m_terrainWarningSound.isPlaying == true)
                {
                    m_terrainWarningSound.Stop();
                }
            }
        }
    }

    protected virtual void HudRot()
    {
        m_hudRot.rotation = Quaternion.Euler(0, 0, -m_player.ZRot);
    }

    void OnGUI()
    {
        GameObject[] m_viableTargets;

        m_viableTargets = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject m_viable in m_viableTargets)
        {
            float m_dist;
            Vector3 m_targetHudPos;
            Rect m_targetRect;

            m_targetHudPos = m_camera.WorldToScreenPoint(m_viable.transform.position);
            m_dist = Vector3.Distance(m_viable.transform.position, m_playerObject.transform.position);

            if (m_dist <= 10000)
            {
                if (m_targetHudPos.z >= 0)
                {
                    if (m_controller.SPOn == false)
                    {
                        if (m_viable.layer == LayerMask.NameToLayer("Air Enemy"))
                        {
                            m_targetRect = new Rect(m_targetHudPos.x - m_viableTarget.width / 2, Screen.height - m_targetHudPos.y - m_viableTarget.height / 2, m_viableTarget.width, m_viableTarget.height);
                            GUI.DrawTexture(m_targetRect, m_viableTarget);
                        }
                        else if (m_viable.layer == LayerMask.NameToLayer("Ground Enemy"))
                        {
                            m_targetRect = new Rect(m_targetHudPos.x - m_viableTarget.width / 2, Screen.height - m_targetHudPos.y - m_viableTarget.height / 2, m_viableTarget.width, m_viableTarget.height);
                            GUI.DrawTexture(m_targetRect, m_noTarget);
                        }
                    }
                    else if (m_controller.SPOn == true)
                    {
                        if (m_viable.layer == LayerMask.NameToLayer("Ground Enemy"))
                        {
                            if (m_viable.GetComponent<MissionObjective>() != null)
                            {
                                m_targetRect = new Rect(m_targetHudPos.x - m_viableTarget.width / 2, Screen.height - m_targetHudPos.y - m_viableTarget.height / 2, m_viableTarget.width, m_viableTarget.height);
                                GUI.DrawTexture(m_targetRect, m_viableTGTTarget);
                            }
                            else
                            {
                                m_targetRect = new Rect(m_targetHudPos.x - m_viableTarget.width / 2, Screen.height - m_targetHudPos.y - m_viableTarget.height / 2, m_viableTarget.width, m_viableTarget.height);
                                GUI.DrawTexture(m_targetRect, m_viableTarget);
                            }
                        }
                        else if (m_viable.layer == LayerMask.NameToLayer("Air Enemy"))
                        {
                            m_targetRect = new Rect(m_targetHudPos.x - m_viableTarget.width / 2, Screen.height - m_targetHudPos.y - m_viableTarget.height / 2, m_viableTarget.width, m_viableTarget.height);
                            GUI.DrawTexture(m_targetRect, m_noTarget);
                        }
                    }
                }
            }
            
        }
    }
}
