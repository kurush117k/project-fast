﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour
{

    protected BaseFlight m_playerFlight;

	// Use this for initialization
	void Start ()
    {
        Initialization();
    }

    protected virtual void Initialization()
    {
        m_playerFlight = this.gameObject.GetComponent<BaseFlight>();
    }

    // Update is called once per frame
    void Update ()
    {
        Controls();
    }

    protected virtual void Controls()
    {
        //PC
        //ROLL
        if (Input.GetKey(KeyCode.A))
        {
            m_playerFlight.Roll(1.0f);
        }
        if (Input.GetKey(KeyCode.D))
        {
            m_playerFlight.Roll(-1.0f);
        }

        //Pitch
        if (Input.GetKey(KeyCode.W))
        {
            m_playerFlight.Pitch(1.0f);
        }
        if (Input.GetKey(KeyCode.S))
        {
            m_playerFlight.Pitch(-1.0f);
        }

        //YAW
        if (Input.GetKey(KeyCode.Q))
        {
            m_playerFlight.Yaw(-1.0f);
        }
        if (Input.GetKey(KeyCode.E))
        {
            m_playerFlight.Yaw(1.0f);
        }

        //360
        //ROll
        m_playerFlight.Roll(-Input.GetAxis("Roll"));
        //PITCH
        m_playerFlight.Pitch(-Input.GetAxis("Pitch"));
        //YAW
        if (Input.GetButton("Yaw Left"))
        {
            m_playerFlight.Yaw(-1.0f);
        }
        else if (Input.GetButton("Yaw Right"))
        {
            m_playerFlight.Yaw(1.0f);
        }
        //Decelerate
        if (Input.GetAxis("Decelerate") > 0.0f)
        {
            m_playerFlight.Decelerate(Input.GetAxis("Decelerate"));
        }
        //Accelerate
        else if (Input.GetAxis("Accelerate") > 0.0f)
        {
            m_playerFlight.Accelerate(Input.GetAxis("Accelerate"));
        }
        //Defult Speed
        else
        {
            m_playerFlight.DefaultSpeed();
        }
    }
}
