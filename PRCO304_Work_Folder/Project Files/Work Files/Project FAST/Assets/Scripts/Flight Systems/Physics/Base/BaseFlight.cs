﻿using UnityEngine;
using System.Collections;

public class BaseFlight : MonoBehaviour
{
    //[SerializeField]
    //protected Transform m_trustPoint;
    //[SerializeField]
    //protected Transform m_pitchPoint;
    //[SerializeField]
    //protected Transform m_rollPoint;
    //[SerializeField]
    //protected Transform m_yawPoint;

    protected float m_weight;
    protected float m_maxVelocity;
    protected float m_minVelocity;
    protected float m_defualtVelocity;
    protected float m_velocity;
    protected float m_baseVelocity;
    protected float m_pitch;
    protected float m_roll;
    protected float m_yaw;
    protected float m_drag;
    protected float m_accelerationRate;
    protected float m_decelerationRate;

    protected GameObject m_jet;
    protected Rigidbody m_airFrame;
    protected Transform m_airFrameTransform;

    protected ParticleSystem[] m_affterburnerEffects;

	// Use this for initialization
	void Start ()
    {
        Initialization();
    }

    protected virtual void Initialization()
    {
        m_weight = 1.0f;
        m_velocity = 350.0f;
        m_defualtVelocity = m_velocity;
        m_maxVelocity = 800.0f;
        m_minVelocity = 200.0f;
        m_pitch = 100.0f;
        m_roll = 100.0f;
        m_yaw = 20.0f;
        m_drag = 5.0f;
        m_accelerationRate = 50.0f;
        m_decelerationRate = 50.0f;

        m_jet = this.gameObject;
        m_airFrame = m_jet.GetComponent<Rigidbody>();
        m_airFrameTransform = m_jet.GetComponent<Transform>();
        m_affterburnerEffects = m_jet.GetComponentsInChildren<ParticleSystem>();

        m_airFrame.drag = m_drag;
        //m_airFrame.mass = m_weight;
    }

    // Update is called once per frame
    void Update ()
    {
        Thrust();
    }

    protected virtual void Thrust()
    {
        m_velocity = Mathf.Clamp(m_velocity, m_minVelocity, m_maxVelocity);

        m_airFrame.AddRelativeForce(Vector3.forward * m_velocity * Time.deltaTime);
    }

    public virtual void Decelerate(float m_decelerateInput)
    {
        m_velocity = m_velocity - (m_decelerationRate * m_decelerateInput * Time.deltaTime);
    }

    public virtual void Accelerate(float m_accelerateInput)
    {
        m_velocity = m_velocity + (m_accelerationRate * m_accelerateInput * Time.deltaTime);
    }

    public virtual void DefaultSpeed()
    {
        if (m_velocity > m_defualtVelocity)
        {
            if (m_velocity < m_defualtVelocity + 1)
            {
                m_velocity = m_defualtVelocity;
            }
            else
            {
                m_velocity = m_velocity - (0.66f * m_decelerationRate * Time.deltaTime);
            }
        }
        else if (m_velocity < m_defualtVelocity)
        {
            if (m_velocity > m_defualtVelocity - 1)
            {
                m_velocity = m_defualtVelocity;
            }
            else
            {
                m_velocity = m_velocity + (0.66f * m_accelerationRate * Time.deltaTime);
            }
        }
    }

    public virtual void Pitch(float m_pitchInput)
    {
        m_airFrameTransform.Rotate(Vector3.right, m_pitch * m_pitchInput * Time.deltaTime);
        //m_airFrameTransform.RotateAround(m_pitchPoint.position, Vector3.right, m_pitch * m_pitchInput * Time.deltaTime);
    }

    public virtual void Roll(float m_rollInput)
    {
        m_airFrameTransform.Rotate(Vector3.forward, m_roll * m_rollInput * Time.deltaTime);
        //m_airFrameTransform.RotateAround(m_rollPoint.position, Vector3.forward, m_roll * m_rollInput * Time.deltaTime);
    }

    public virtual void Yaw(float m_yawInput)
    {
        m_airFrameTransform.Rotate(Vector3.up, m_yaw * m_yawInput * Time.deltaTime);
        //m_airFrameTransform.RotateAround(m_yawPoint.position, Vector3.up, m_yaw * m_yawInput * Time.deltaTime);
    }

    public virtual void Level()
    {
        Vector3 m_position;
        Vector3 m_levelPosition;

        m_position.x = m_airFrameTransform.rotation.x;
        m_position.y = m_airFrameTransform.rotation.y;
        m_position.z = m_airFrameTransform.rotation.z;

        m_levelPosition = new Vector3(0, m_position.y, 0);

        m_airFrameTransform.Rotate(Vector3.Slerp(m_position, m_levelPosition, 3.0f));
    }

    protected virtual void AfterBurner()
    {
        foreach (ParticleSystem m_afterBurner in m_affterburnerEffects)
        {
            var m_em = m_afterBurner.emission;
            var m_rate = new ParticleSystem.MinMaxCurve();

            m_rate.constantMax = m_velocity /10;
            m_em.rate = m_rate;
        }
    }

    void OnCollisionEnter(Collision m_collision)
    {
        m_airFrame.velocity = Vector3.zero;
        m_airFrame.angularVelocity = Vector3.zero;
    }

    public float Velocity
    {
        get
        {
            return m_velocity;
        }
    }
}
