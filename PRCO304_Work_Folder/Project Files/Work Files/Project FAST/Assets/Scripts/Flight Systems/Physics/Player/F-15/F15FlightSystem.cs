﻿using UnityEngine;
using System.Collections;

public class F15FlightSystem : BaseFlight
{
    protected override void Initialization()
    {
        m_weight = 12700.0f;
        m_velocity = 70000.0f;
        m_defualtVelocity = m_velocity;
        m_maxVelocity = 250000.0f;
        m_minVelocity = 55000.0f;
        m_pitch = 100.0f;
        m_roll = 100.0f;
        m_yaw = 20.0f;
        m_drag = 5.0f;
        m_accelerationRate = 7500.0f;
        m_decelerationRate = 7500.0f;

        m_jet = this.gameObject;
        m_airFrame = m_jet.GetComponent<Rigidbody>();
        m_airFrameTransform = m_jet.GetComponent<Transform>();

        m_airFrame.drag = m_drag;
        //m_airFrame.mass = m_weight;
    }
}
