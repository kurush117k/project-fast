﻿using UnityEngine;
using System.Collections;

public class AIFlight : BaseFlight 
{
    protected override void Initialization()
    {
        m_weight = 1.0f;
        m_velocity = 350.0f;
        m_defualtVelocity = m_velocity;
        m_maxVelocity = 800.0f;
        m_minVelocity = 200.0f;
        m_pitch = 100.0f;
        m_roll = 100.0f;
        m_yaw = 20.0f;
        m_drag = 5.0f;
        m_accelerationRate = 50.0f;
        m_decelerationRate = 50.0f;

        m_jet = this.gameObject;
        m_airFrame = m_jet.GetComponent<Rigidbody>();
        m_airFrameTransform = m_jet.GetComponent<Transform>();

        m_airFrame.drag = m_drag;
        m_airFrame.mass = m_weight;
    }

    public virtual void TargetIntercept(Transform m_target, float m_interceptInput)
    {
        Quaternion m_targetLook;

        m_targetLook = Quaternion.LookRotation(m_target.position - m_jet.transform.position);

        m_airFrame.MoveRotation(Quaternion.Slerp(m_airFrame.rotation, m_targetLook, Time.deltaTime * m_interceptInput));
    }
}
