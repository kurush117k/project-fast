﻿using UnityEngine;
using System.Collections;

public class SU35AIFlight : AIFlight
{
    protected override void Initialization()
    {
        m_weight = 1.0f;
        m_velocity = 70000.0f;
        m_defualtVelocity = m_velocity;
        m_maxVelocity = 250000.0f;
        m_minVelocity = 55000.0f;
        m_pitch = 100.0f;
        m_roll = 100.0f;
        m_yaw = 20.0f;
        m_drag = 5.0f;
        m_accelerationRate = 5000.0f;
        m_decelerationRate = 5000.0f;

        m_jet = this.gameObject;
        m_airFrame = m_jet.GetComponent<Rigidbody>();
        m_airFrameTransform = m_jet.GetComponent<Transform>();

        m_airFrame.drag = m_drag;
        m_airFrame.mass = m_weight;
    }
}
