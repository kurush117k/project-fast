﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Load_Level : MonoBehaviour
{
    public void PlayDemo(string m_levelName)
    {
        SceneManager.LoadScene(m_levelName);
    }

    public void Controls(string m_levelName)
    {
        SceneManager.LoadScene(m_levelName);
    }

    public void Confirm(string m_levelName)
    {
        SceneManager.LoadScene(m_levelName);
    }

    public void Cancel(string m_levelName)
    {
        SceneManager.LoadScene(m_levelName);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
