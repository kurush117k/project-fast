﻿using UnityEngine;
using System.Collections;

public class Cont : MonoBehaviour
{
    protected BaseAutocannon m_autocannon;
    protected BaseRocketPod m_rocketPod;
    protected BombDrop m_bombDrop;
    protected BaseMissileLauncher m_missileLauncher;

    protected GameObject m_player;

    protected bool m_spOn;

    // Use this for initialization
    void Start()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_player = this.gameObject;
        m_autocannon = m_player.GetComponentInChildren<BaseAutocannon>();
        m_rocketPod = m_player.GetComponentInChildren<BaseRocketPod>();
        m_bombDrop = m_player.GetComponentInChildren<BombDrop>();
        m_missileLauncher = m_player.GetComponentInChildren<BaseMissileLauncher>();

        m_spOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        Control();
        //Debug.Log(m_spOn);
    }

    protected virtual void Control()
    {
        if (Input.GetButton("Fire"))
        {
            m_autocannon.AutocannonFire();
        }
        if (Input.GetKey(KeyCode.Space))
        {
            m_autocannon.AutocannonFire();
        }

        if (Input.GetButton("Switch Weapon"))
        {
            if (m_spOn == false)
            {
                m_spOn = true;
            }
            else if (m_spOn == true)
            {
                m_spOn = false;
            }
        }

        if (m_spOn == false)
        {
            if (Input.GetButton("Launch"))
            {
                m_rocketPod.RocketSalvo();
            }

            //if (Input.GetKey(KeyCode.Space))
            //{
            //    
            //}
        }
        if (m_spOn == true)
        {
            if (Input.GetButton("Launch"))
            {
                m_bombDrop.Launch();
            }

            //if (Input.GetKey(KeyCode.Space))
            //{
            //    
            //}
        }

        if (Input.GetKey(KeyCode.Space))
        {
            m_missileLauncher.MissileLaunch();
        }
    }
}
