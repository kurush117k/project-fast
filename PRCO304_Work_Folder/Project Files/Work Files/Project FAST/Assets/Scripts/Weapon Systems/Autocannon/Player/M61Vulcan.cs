﻿using UnityEngine;
using System.Collections;

public class M61Vulcan : PlayerAutoCannon
{
    public override void Initialization()
    {
        m_fireRate = 10.0f;
        m_fireTimer = 1.0f;
        m_heatCollected = 0.0f;
        m_heatPercent = 100.0f;
        m_heatPerShoot = 2.5f;
        m_maxCoolTime = 10.0f;
        m_maxRange = 1000.0f;

        m_maxAmmo = 1000;
        m_ammoLeft = m_maxAmmo;
        m_damage = 5;
        m_maxArraySize = (int)(m_heatPercent / m_heatPerShoot);

        m_targetTag = "Enemy";

        LayerMask m_tempOne = 1 << 9;
        LayerMask m_tempTwo = 1 << 10;

        m_mask = m_tempOne | m_tempTwo;

        m_overHeated = false;
        m_cooling = false;
    }
}
