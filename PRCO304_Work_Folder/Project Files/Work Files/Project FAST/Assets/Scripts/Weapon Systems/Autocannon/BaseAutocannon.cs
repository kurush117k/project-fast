﻿using UnityEngine;
using System.Collections;

public class BaseAutocannon : MonoBehaviour
{
    [SerializeField]
    protected Transform m_muzzle;
    [SerializeField]
    protected AudioSource m_fireSound;
    [SerializeField]
    protected AudioSource m_hitSound;
    [SerializeField]
    protected AudioSource m_coolDownSound;
    [SerializeField]
    protected ParticleSystem m_muzzleFlash;
    [SerializeField]
    protected ParticleSystem m_bulletHit;
    protected ParticleSystem[] m_bulletHitArray;
    [SerializeField]
    protected ParticleSystem m_coolDownEffect;
    [SerializeField]
    protected ParticleSystem m_bulletTracerEffect;

    protected float m_fireRate;
    protected float m_fireTimer;
    protected float m_maxRange;

    protected int m_maxAmmo;
    protected int m_damage;
    protected int m_maxArraySize;

    protected string m_targetTag;

    protected LayerMask m_mask;

    protected RaycastHit m_hit;
    protected Ray m_bulletTrajectory;

    

    // Use this for initialization
    void Start ()
    {
        Initialization();
        EffectsPool();
    }

    public virtual void Initialization()
    {
        m_fireRate = 10.0f;
        m_fireTimer = 1.0f;
        m_maxRange = 1000.0f;

        m_maxAmmo = 1000;
        m_damage = 5;
        m_maxArraySize = (int)(m_maxAmmo / m_fireRate);

        m_targetTag = "Defualt";

        m_mask = 1 << 1;
    }

    protected virtual void EffectsPool()
    {
        if (m_bulletHit)
        {
            m_bulletHitArray = new ParticleSystem[m_maxArraySize];

            for (int i = 0; i < m_maxArraySize; i++)
            {
                m_bulletHitArray[i] = Instantiate(m_bulletHit) as ParticleSystem;
                m_bulletHitArray[i].gameObject.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update ()
    {
        
    }

    public virtual void AutocannonFire()
    {
        if (m_fireTimer < -5)
        {
            m_fireTimer = -5;
        }

        if (Time.timeScale != 0)
        {
            if (m_fireTimer <= 0)
            {
                DetectHit();

                if (m_muzzleFlash)
                {
                    m_muzzleFlash.transform.position = m_muzzle.position;
                    m_muzzleFlash.Play();
                }


                if (m_fireSound)
                {
                    m_fireSound.Play();
                }
                if (m_bulletTracerEffect)
                {
                    //m_bulletTracerEffect.transform.position = m_muzzle.position;
                    m_bulletTracerEffect.Play();
                }

                m_fireTimer = 1;
            }
        }

        m_fireTimer -= Time.deltaTime * m_fireRate;
    }

    protected virtual void DetectHit()
    {
        GameObject m_targetObject;
       
        m_bulletTrajectory = new Ray(m_muzzle.position, m_muzzle.forward);

        if (Physics.Raycast(m_bulletTrajectory, out m_hit, m_maxRange, m_mask))
        {
            Debug.Log("Hit");
            if (m_hit.collider.tag == m_targetTag)
            {
                m_targetObject = m_hit.collider.gameObject;

                m_targetObject.GetComponent<BaseObject>().Damage(m_damage);
            }

            if (m_bulletHitArray != null)
            {
                for (int n = 0; n < m_maxArraySize; n++)
                {
                    if (m_bulletHitArray[n].gameObject.activeSelf == false)
                    {
                        m_bulletHitArray[n].gameObject.SetActive(true);
                        m_bulletHitArray[n].transform.position = m_hit.point;
                        m_bulletHitArray[n].transform.rotation = Quaternion.identity;
                        m_bulletHitArray[n].Play();

                        if (m_hitSound)
                        {
                            m_hitSound.Play();
                        }

                        break;
                    }
                }
            }
        }
    }
}
