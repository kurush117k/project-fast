﻿using UnityEngine;
using System.Collections;

public class PlayerAutoCannon : BaseAutocannon
{
    protected float m_heatCollected;
    protected float m_heatPercent;
    protected float m_heatPerShoot;
    protected float m_maxCoolTime;

    protected float m_targetRadius;

    protected int m_ammoLeft;

    protected bool m_overHeated;
    protected bool m_cooling;
    protected bool m_targetSight;

    protected RaycastHit m_targetHit;
    protected Ray m_targetRay;
    protected Vector3 m_target;

    public override void Initialization()
    {
        m_fireRate = 10.0f;
        m_fireTimer = 1.0f;
        m_heatCollected = 0.0f;
        m_heatPercent = 100.0f;
        m_heatPerShoot = 2.5f;
        m_maxCoolTime = 10.0f;
        m_maxRange = 1000.0f;

        m_targetRadius = 3.0f;

        m_maxAmmo = 1000;
        m_ammoLeft = m_maxAmmo;
        m_damage = 5;
        m_maxArraySize = (int)(m_heatPercent / m_heatPerShoot);

        m_targetTag = "Enemy";

        LayerMask m_tempOne = 1 << 9;
        LayerMask m_tempTwo = 1 << 10;

        m_mask = m_tempOne | m_tempTwo;

        m_overHeated = false;
        m_cooling = false;
        m_targetSight = false;
    }

    // Update is called once per frame
    void Update()
    {
        Target();
        AutocannonOverHeat();
    }

    public override void AutocannonFire()
    {
        if (m_ammoLeft > 0 && m_overHeated == false)
        {
            if (m_fireTimer < -5)
            {
                m_fireTimer = -5;
            }

            if (Time.timeScale != 0)
            {
                if (m_fireTimer <= 0)
                {
                    DetectHit();
                    m_ammoLeft = m_ammoLeft - 1;
                    m_heatCollected = m_heatCollected + m_heatPerShoot;

                    if (m_muzzleFlash)
                    {
                        m_muzzleFlash.transform.position = m_muzzle.position;
                        m_muzzleFlash.Play();
                    }

                    if (m_fireSound)
                    {
                            m_fireSound.Play();
                    }

                    if (m_bulletTracerEffect)
                    {
                        m_bulletTracerEffect.transform.position = m_muzzle.position;
                        m_bulletTracerEffect.Play();
                    }

                    

                    if (m_cooling == false)
                    {
                        StartCoroutine(AutoCool());
                    }

                    m_fireTimer = 1;
                }
            }
        }

        m_fireTimer -= Time.deltaTime * m_fireRate;
    }

    protected virtual void AutocannonOverHeat()
    {
        if (m_heatCollected >= m_heatPercent && m_overHeated == false)
        {
            m_overHeated = true;

            if (m_coolDownSound)
            {
                m_coolDownSound.Play();
            }
            if (m_coolDownEffect)
            {
                m_coolDownEffect.Play();
            }

            StartCoroutine(Cooldown());
        }
    }

    protected virtual void Target()
    {
        m_targetRay = new Ray(m_muzzle.position, m_muzzle.forward);

        if (Physics.SphereCast(m_targetRay, 3.0f, m_maxRange, m_mask))
        {
            m_targetSight = true;
        }
        else
        {
            m_targetSight = false;
        }

        if (Physics.Raycast(m_targetRay, out m_targetHit, m_maxRange, m_mask))
        {
            m_target = new Vector3(m_targetHit.point.x, m_targetHit.point.y, m_targetHit.point.z);
        }
        else
        {
            m_target = new Vector3(0.0f, 0.0f, 0.0f);
        }
    }

    protected IEnumerator Cooldown()
    {
        float m_time;

        m_time = 0.0f;

        while (m_heatCollected > 0)
        {
            m_time += Time.deltaTime;

            if (m_time >= (m_maxCoolTime / m_heatPercent))
            {
                m_heatCollected -= 1;
                m_time = 0.0f;
            }

            yield return null;
        }

        m_time = 0.0f;
        m_overHeated = false;
    }

    protected IEnumerator AutoCool()
    {
        float m_time;

        m_cooling = true;
        m_time = 0.0f;

        while (m_heatCollected > 0)
        {
            m_time += Time.deltaTime;

            if (m_time > 1)
            {
                if (m_overHeated == false)
                {
                    m_heatCollected -= 2;
                    m_time = 0.0f;
                }
            }

            yield return null;
        }

        m_time = 0.0f;
        m_cooling = false;
    }

    public Vector3 Hit
    {
        get
        {
            return m_target;
        }
    }

    public int Ammo
    {
        get
        {
            return m_ammoLeft;
        }
    }

    public float Heat
    {
        get
        {
            return m_heatCollected;
        }
    }

    public bool TargetSight
    {
        get
        {
            return m_targetSight;
        }
    }
}
