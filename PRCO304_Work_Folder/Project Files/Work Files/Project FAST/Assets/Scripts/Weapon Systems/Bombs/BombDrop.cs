﻿using UnityEngine;
using System.Collections;

public class BombDrop : MonoBehaviour
{
    [SerializeField]
    protected GameObject m_bomb;
    protected GameObject[] m_bombArray;
    [SerializeField]
    protected GameObject m_dummyBomb;
    protected GameObject[] m_dummyBombArray;
    [SerializeField]
    protected AudioSource m_launchSound;

    protected GameObject m_bombLauncher;
    protected Transform[] m_bombLauncherArray;

    protected BaseFlight m_flight;

    protected float m_armTime;

    protected int m_bombAmmount;

    protected bool m_bombArmed;

    // Use this for initialization
    void Start()
    {
        Initialization();
        BombPool();
        DummyBombPool();
    }

    public virtual void Initialization()
    {
        m_bombLauncher = this.gameObject;
        m_bombLauncherArray = m_bombLauncher.GetComponentsInChildren<Transform>();

        m_flight = m_bombLauncher.GetComponentInParent<BaseFlight>();

        m_armTime = 5.0f;

        m_bombAmmount = m_bombLauncherArray.Length;

        m_bombArmed = true;
    }

    protected virtual void BombPool()
    {
        if (m_bomb)
        {
            m_bombArray = new GameObject[m_bombAmmount];

            for (int i = 0; i < m_bombAmmount; i++)
            {
                m_bombArray[i] = Instantiate(m_bomb) as GameObject;
                m_bombArray[i].SetActive(false);
            }
        }
    }

    protected virtual void DummyBombPool()
    {
        if (m_dummyBomb)
        {
            m_dummyBombArray = new GameObject[m_bombAmmount];

            for (int i = 0; i < m_bombAmmount; i++)
            {
                m_dummyBombArray[i] = Instantiate(m_dummyBomb) as GameObject;
                m_dummyBombArray[i].transform.parent = m_bombLauncher.transform;
                m_dummyBombArray[i].SetActive(true);
                m_dummyBombArray[i].transform.position = m_bombLauncherArray[i].transform.position;
                m_dummyBombArray[i].transform.rotation = m_bombLauncherArray[i].transform.rotation;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void Launch()
    {
        if (m_bombArmed == true)
        {
            int m_armed;

            m_armed = m_bombAmmount;

            while (m_armed > 0)
            {
                for (int n = 0; n < m_bombAmmount; n++)
                {
                    if (m_bombArray[n].activeSelf == false)
                    {
                        m_dummyBombArray[n].SetActive(false);
                        m_bombArray[n].SetActive(true);
                        m_bombArray[n].GetComponent<BaseBomb>().Initialization(m_flight.Velocity);
                        m_bombArray[n].transform.position = m_bombLauncherArray[n].position;
                        m_bombArray[n].transform.rotation = m_bombLauncherArray[n].rotation;

                        if (m_launchSound)
                        {
                            m_launchSound.Play();
                        }

                        m_armed -= 1;

                        break;
                    }
                }
            }

            m_bombArmed = false;
            StartCoroutine(Arm());
        }
    }

    protected IEnumerator Arm()
    {
        yield return new WaitForSeconds(m_armTime);

        for (int n = 0; n < m_bombAmmount; n++)
        {
            m_dummyBombArray[n].SetActive(true);
            m_dummyBombArray[n].transform.position = m_bombLauncherArray[n].transform.position;
            m_dummyBombArray[n].transform.rotation = m_bombLauncherArray[n].transform.rotation;
        }

        m_bombArmed = true;
    }

    public bool Armed
    {
        get
        {
            return m_bombArmed;
        }
    }
}
