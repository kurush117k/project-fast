﻿using UnityEngine;
using System.Collections;

public class BaseBomb : MonoBehaviour
{
    [SerializeField]
    protected ParticleSystem m_bombExplosionEffect;
    [SerializeField]
    protected AudioSource m_bombExplosionSound;

    protected GameObject m_bomb;
    protected Rigidbody m_bombBody;

    protected float m_damageRadius;
    protected float m_bombWeight;
    protected float m_velocity;

    protected int m_damage;

    protected bool m_contact;

    protected LayerMask m_objectMask;

    // Use this for initialization
    void Start ()
    {
        //Initialization();
    }

    public virtual void Initialization(float m_planeVelocity)
    {
        m_bomb = this.gameObject;
        m_bombBody = m_bomb.GetComponent<Rigidbody>();

        m_damageRadius = 10.0f;
        m_bombWeight = 10.0f;
        m_velocity = m_planeVelocity;

        m_damage = 100;

        m_contact = false;

        m_objectMask = 1 << 9;

        m_bombBody.mass = m_bombWeight;
        m_bombBody.useGravity = true;
        m_bombBody.isKinematic = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Destroy();
    }

    protected virtual void Destroy()
    {
        if (m_contact == true)
        {
            Collider[] m_objectsHit;

            m_objectsHit = Physics.OverlapSphere(m_bomb.transform.position, m_damageRadius, m_objectMask);

            if (m_bombExplosionEffect)
            {
                m_bombExplosionEffect.Play();
            }

            if (m_bombExplosionSound)
            {
                m_bombExplosionSound.Play();
            }

            foreach (Collider m_object in m_objectsHit)
            {
                m_object.gameObject.GetComponent<BaseObject>().Damage(m_damage);
            }

            m_bomb.SetActive(false);
            m_bombBody.useGravity = false;
            m_bombBody.isKinematic = true;
        }
    }

    void OnCollisionEnter(Collision m_collision)
    {
        m_contact = true;
    }
}
