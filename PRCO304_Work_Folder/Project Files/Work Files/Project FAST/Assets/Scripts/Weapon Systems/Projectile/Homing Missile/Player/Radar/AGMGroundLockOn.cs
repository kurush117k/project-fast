﻿using UnityEngine;
using System.Collections;

public class AGMGroundLockOn : BaseMissileLockOn 
{
    public override void Initialization()
    {
        m_radar = this.gameObject;

        m_radarPosition = m_radar.GetComponent<Transform>();
        m_radarRange = 2000.0f;
        m_lockonRange = 1700.0f;
        m_radarAngle = 40.0f;

        m_targetMask = 1 << 10;
    }
}
