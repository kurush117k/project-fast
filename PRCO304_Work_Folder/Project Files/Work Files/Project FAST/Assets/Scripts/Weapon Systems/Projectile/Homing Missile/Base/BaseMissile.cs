﻿using UnityEngine;
using System.Collections;

public class BaseMissile : BaseProjectile
{
    protected GameObject m_target;
    [SerializeField]
    protected Transform m_missileTip;

    protected float m_trajectoryDamping;
    protected float m_targetMaxAngle;
    protected float m_thrustDelay;

    protected bool m_tracking;

    protected RaycastHit m_hit;

    protected Ray m_missileTargetRangeRay;

    public virtual void Initialization(float m_currentVelocity, GameObject m_radarTarget)
    {
        m_projectile = this.gameObject;
        m_projectileBody = m_projectile.GetComponent<Rigidbody>();
        m_projectileBody.isKinematic = false;

        m_projectileTrail = m_projectile.GetComponent<TrailRenderer>();

        m_target = m_radarTarget;

        m_flightTime = 5.0f;
        m_accelerationRate = 200.0f;
        //m_velocity = m_currentVelocity / 100;
        m_maxVelocity = 1000.0f;
        m_damageRadius = 50.0f;
        m_targetMaxAngle = 45.0f;
        m_trajectoryDamping = 5.0f;
        m_thrustDelay = 0.5f;

        m_damage = 20;

        m_objectTag = "Default";

        m_flying = true;
        m_contact = false;

        m_objectMask = 1 << 9;

        if (m_thrustEffect)
        {
            m_thrustEffect.Play();
        }

        if (m_thrustSound)
        {
            m_thrustSound.Play();
        }

        if (m_target != null)
        {
            m_tracking = true;
            m_target.GetComponent<BaseObject>().IsTracked = true;
        }
        else if (m_target == null)
        {
            m_tracking = false;
        }

        StartCoroutine(ProjectileGravity());
        StartCoroutine(Thrust());
    }
	
	// Update is called once per frame
	void Update ()
    {
        Acceleration();
        Destroy();
        NearTargetDetonate();

        //Debug.Log(m_missileBody.velocity);
        if (m_tracking == true)
        {
            NearTargetAngle();
            Trajectory();
        }
        else if (m_tracking == false)
        {
            if (m_target != null)
            {
                m_target.GetComponent<BaseObject>().IsTracked = false;
            }
        }
    }

    protected virtual void Trajectory()
    {
        Quaternion m_targetLook;

        m_targetLook = Quaternion.LookRotation(m_target.transform.position - m_projectile.transform.position);
        //m_projectile.transform.rotation = Quaternion.Slerp(m_projectile.transform.rotation, m_targetLook, Time.deltaTime * m_trajectoryDamping);
        m_projectileBody.MoveRotation(Quaternion.Slerp(m_projectile.transform.rotation, m_targetLook, Time.deltaTime * m_trajectoryDamping));
    }

    protected virtual void NearTargetDetonate()
    {
        float m_targetDist;

        if (m_target != null)
        {
            m_targetDist = Vector3.Distance(m_target.transform.position, m_missileTip.position);

            if (m_targetDist <= m_damageRadius)
            {
                m_contact = true;
            }
        }
    }

    protected override void Destroy()
    {
        if (m_flying == false && m_contact == false)
        {
            m_projectile.SetActive(false);
            if (m_target != null)
            {
                m_target.gameObject.GetComponent<BaseObject>().IsTracked = false;
            }
            m_projectileBody.velocity = Vector3.zero;
            m_projectileBody.isKinematic = true;
            m_projectileTrail.Clear();
        }
        else if (m_flying == true && m_contact == true)
        {
            Collider[] m_objectHit;

            m_objectHit = Physics.OverlapSphere(m_projectile.transform.position, m_damageRadius, m_objectMask);

            if (m_explosionEffect)
            {
                Debug.Log("Go BOOM BOOM");
                m_explosionEffect.gameObject.transform.position = m_projectile.transform.position;
                m_explosionEffect.SetActive(true);
            }

            if (m_explosionSound)
            {
                m_explosionSound.Play();
            }

            if (m_objectHit != null)
            {
                foreach (Collider m_object in m_objectHit)
                {
                    if (m_object.gameObject.tag == m_objectTag)
                    {
                        m_object.gameObject.GetComponent<BaseObject>().Damage(m_damage);
                    }
                }
            }

            m_projectile.SetActive(false);
            if (m_target != null)
            {
                m_target.gameObject.GetComponent<BaseObject>().IsTracked = false;
            }
            m_projectileBody.velocity = Vector3.zero;
            m_projectileBody.isKinematic = true;
            m_projectileTrail.Clear();
        }
    }

    protected virtual void NearTargetAngle()
    {
        Vector3 m_targerDirection;
        float m_targetAngle;

        m_targerDirection = m_target.transform.position - m_missileTip.position;

        m_targetAngle = Vector3.Angle(m_targerDirection, m_missileTip.forward);

        if (m_targetAngle < m_targetMaxAngle)
        {
            m_tracking = true;
        }
        else
        {
            m_tracking = false;
        }
    }

    protected IEnumerator ProjectileGravity()
    {
        yield return new WaitForSeconds(m_thrustDelay);
        m_projectileBody.useGravity = false;

        if (m_target != null)
        {
            m_tracking = true;
        }
    }
}
