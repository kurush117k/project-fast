﻿using UnityEngine;
using System.Collections;

public class BaseMissileLauncher : MonoBehaviour {

    [SerializeField]
    protected GameObject m_missile;
    protected GameObject[] m_missileArray;
    [SerializeField]
    protected GameObject m_dummyMissile;
    protected GameObject[] m_dummyMissileArray;
    [SerializeField]
    protected AudioSource m_launchSound;

    protected BaseFlight m_flight;
    protected GameObject m_radar;
    protected BaseMissileLockOn m_radarLockOn;
    protected GameObject m_jetObject;

    protected GameObject m_launchPoint;
    protected Transform[] m_launchPointArray;

    protected float m_armTime;

    protected int m_missilePayload;
    
    protected bool m_missileArmed;

    protected Collider[] m_colliderArray;

    // Use this for initialization
    void Start ()
    {
        Initialization();
        MissilePool();
        DummyMissilePool();
    }

    public virtual void Initialization()
    {
        m_launchPoint = this.gameObject;
        m_launchPointArray = m_launchPoint.GetComponentsInChildren<Transform>();
        m_radar = m_launchPoint.transform.parent.gameObject;
        m_jetObject = m_radar.transform.parent.gameObject;

        m_flight = m_launchPoint.GetComponentInParent<BaseFlight>();
        m_radarLockOn = m_launchPoint.GetComponentInParent<BaseMissileLockOn>();
        m_colliderArray = m_jetObject.GetComponentsInChildren<Collider>();
        
        m_armTime = 20.0f;

        m_missilePayload = m_launchPointArray.Length;

        m_missileArmed = true;
    }

    protected virtual void MissilePool()
    {
        if (m_missile)
        {
            m_missileArray = new GameObject[m_missilePayload];

            for (int i = 0; i < m_missilePayload; i++)
            {
                m_missileArray[i] = Instantiate(m_missile) as GameObject;
                
                foreach (Collider m_collider in m_colliderArray)
                {
                    Physics.IgnoreCollision(m_missileArray[i].GetComponent<Collider>(), m_collider);
                }

                m_missileArray[i].SetActive(false);
            }
        }
    }

    protected virtual void DummyMissilePool()
    {
        if (m_dummyMissile)
        {
            m_dummyMissileArray = new GameObject[m_missilePayload];

            for (int i = 0; i < m_missilePayload; i++)
            {
                m_dummyMissileArray[i] = Instantiate(m_dummyMissile) as GameObject;
                m_dummyMissileArray[i].transform.parent = m_launchPoint.transform;
                m_dummyMissileArray[i].SetActive(true);
                m_dummyMissileArray[i].transform.position = m_launchPointArray[i].position;
                m_dummyMissileArray[i].transform.rotation = m_launchPointArray[i].rotation;
            }
        }
    }

    // Update is called once per frame
    void Update ()
    {
        
    }

    public virtual void MissileLaunch()
    {
        if (m_missileArmed == true)
        {
            for (int n = 0; n < m_missilePayload; n++)
            {
                if (m_missileArray[n].activeSelf == false)
                {
                    m_dummyMissileArray[n].SetActive(false);
                    m_missileArray[n].SetActive(true);

                    foreach (Collider m_collider in m_colliderArray)
                    {
                        Physics.IgnoreCollision(m_missileArray[n].GetComponent<Collider>(), m_collider);
                    }

                    m_missileArray[n].GetComponent<BaseMissile>().Initialization(m_flight.Velocity, m_radarLockOn.TargetLockedOn);
                    m_missileArray[n].transform.position = m_launchPointArray[n].position;
                    m_missileArray[n].transform.rotation = m_launchPointArray[n].rotation;

                    m_missileArmed = false;
                    StartCoroutine(Arm(n));

                    break;
                }
            }
        }
    }

    protected IEnumerator Arm(int m_armID)
    {
        yield return new WaitForSeconds(m_armTime);
        m_missileArmed = true;
        m_dummyMissileArray[m_armID].SetActive(true);
        m_dummyMissileArray[m_armID].transform.position = m_launchPointArray[m_armID].position;
        m_dummyMissileArray[m_armID].transform.rotation = m_launchPointArray[m_armID].rotation;
    }

    public bool Armed
    {
        get
        {
            return m_missileArmed;
        }
    }
}
