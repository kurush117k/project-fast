﻿using UnityEngine;
using System.Collections;

public class BaseMissileLockOn : MonoBehaviour
{
    protected GameObject[] m_targetArray;
    protected GameObject m_target;
    protected GameObject m_targetLockedOn;
    
    protected GameObject m_radar;
    protected Transform m_radarPosition;

    protected LayerMask m_targetMask;

    protected float m_radarRange;
    protected float m_lockonRange;
    protected float m_radarAngle;

    // Use this for initialization
    void Start()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_radar = this.gameObject;

        m_radarPosition = m_radar.GetComponent<Transform>();
        m_radarRange = 2000.0f;
        m_lockonRange = 1500.0f;
        m_radarAngle = 35.0f;

        m_targetMask = 1 << 9;
    }

    // Update is called once per frame
    void Update()
    {
        TargetsInRange();
        LockOn();

        if (m_target == null || m_target.activeSelf == false) 
        {
            SelectTarget();
        }
    }

    protected virtual void TargetsInRange()
    {
        Collider[] m_targetsInRange;

        m_targetsInRange = Physics.OverlapSphere(m_radarPosition.position, m_radarRange, m_targetMask);
        m_targetArray = new GameObject[m_targetsInRange.Length];

        for (int i = 0; i < m_targetsInRange.Length; i++)
        {
            m_targetArray[i] = m_targetsInRange[i].gameObject;
        }
    }

    public virtual void SelectTarget()
    {
        if (m_targetArray.Length > 0)
        {
            int m_targetID;

            m_targetID = Random.Range(0, m_targetArray.Length);

            m_target = m_targetArray[m_targetID];
        }
        else if (m_targetArray.Length == 0)
        {
            m_target = null;
        }
    }

    public virtual void PrecisionTarget(Ray m_rayAim)
    {
        RaycastHit m_hit;

        if (m_targetArray.Length > 0)
        {
            if (Physics.SphereCast(m_rayAim, 20.0f, out m_hit, m_radarRange, m_targetMask)) 
            {
                foreach (GameObject m_object in m_targetArray)
                {
                    if (m_object == m_hit.collider.gameObject) 
                    {
                        m_target = m_hit.collider.gameObject;
                    }
                }
            }
        }
    }

    protected virtual void LockOn()
    {
        if (m_target != null)
        {
            Vector3 m_targerDirection;
            float m_targetAngle;
            float m_targetDist;

            m_targetDist = Vector3.Distance(m_target.transform.position, m_radarPosition.position);

            m_targerDirection = m_target.transform.position - m_radarPosition.position;

            m_targetAngle = Vector3.Angle(m_targerDirection, m_radarPosition.forward);

            if (m_targetDist <= m_lockonRange)
            {
                if (m_targetAngle <= m_radarAngle)
                {
                    m_target.GetComponent<BaseObject>().IsLockedOn = true;
                    m_targetLockedOn = m_target;
                }
                else
                {
                    m_target.GetComponent<BaseObject>().IsLockedOn = false;
                    m_targetLockedOn = null;
                }
            }
            else
            {
                m_target.GetComponent<BaseObject>().IsLockedOn = false;
                m_targetLockedOn = null;
            }
        }
    }

    public GameObject Target
    {
        get
        {
            return m_target;
        }
    }

    public GameObject TargetLockedOn
    {
        get
        {
            return m_targetLockedOn;
        }
    }
}
