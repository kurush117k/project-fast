﻿using UnityEngine;
using System.Collections;

public class BaseRocket : BaseProjectile
{
    public override void Initialization(float m_currentVelocity)
    {
        m_projectile = this.gameObject;
        m_projectileBody = m_projectile.GetComponent<Rigidbody>();
        m_projectileBody.isKinematic = false;

        m_projectileTrail = m_projectile.GetComponent<TrailRenderer>();

        m_flightTime = 5.0f;
        m_accelerationRate = 200.0f;
        m_velocity = 0.0f;
        //m_velocity = m_currentVelocity;
        m_maxVelocity = 1000.0f;
        m_damageRadius = 50.0f;

        m_damage = 20;

        m_objectTag = "Default";

        m_flying = true;
        m_contact = false;

        m_objectMask = 1 << 9;

        if (m_thrustEffect)
        {
            m_thrustEffect.Play();
        }

        if (m_thrustSound)
        {
            m_thrustSound.Play();
        }

        StartCoroutine(Thrust());
    }
}
