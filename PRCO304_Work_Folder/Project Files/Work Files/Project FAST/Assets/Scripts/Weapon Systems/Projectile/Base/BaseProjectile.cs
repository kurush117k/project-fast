﻿using UnityEngine;
using System.Collections;

public class BaseProjectile : MonoBehaviour
{
    [SerializeField]
    protected GameObject m_explosionEffect;
    [SerializeField]
    protected AudioSource m_explosionSound;
    [SerializeField]
    protected ParticleSystem m_thrustEffect;
    [SerializeField]
    protected AudioSource m_thrustSound;

    protected GameObject m_projectile;
    protected Rigidbody m_projectileBody;
    protected TrailRenderer m_projectileTrail;

    protected LayerMask m_objectMask;

    protected float m_flightTime;
    protected float m_accelerationRate;
    protected float m_velocity;
    protected float m_maxVelocity;
    protected float m_damageRadius;

    protected int m_damage;

    protected bool m_flying;
    protected bool m_contact;

    protected string m_objectTag; 

	// Use this for initialization
	void Start ()
    {
        SetExplodeEffect();
    }
	
    public virtual void Initialization(float m_currentVelocity)
    {
        m_projectile = this.gameObject;
        m_projectileBody = m_projectile.GetComponent<Rigidbody>();
        m_projectileBody.isKinematic = false;

        m_projectileTrail = m_projectile.GetComponent<TrailRenderer>();

        m_flightTime = 5.0f;
        m_accelerationRate = 200.0f;
        m_velocity = 0.0f;
        //m_velocity = m_currentVelocity;
        m_maxVelocity = 1000.0f;
        m_damageRadius = 50.0f;

        m_damage = 20;

        m_objectTag = "Default";

        m_flying = true;
        m_contact = false;

        m_objectMask = 1 << 9;

        if (m_thrustEffect)
        {
            m_thrustEffect.Play();
        }

        if (m_thrustSound)
        {
            m_thrustSound.Play();
        }

        StartCoroutine(Thrust());
    }

    protected virtual void SetExplodeEffect()
    {
        m_explosionEffect = Instantiate(m_explosionEffect) as GameObject;
        m_explosionEffect.SetActive(false);
    }

	// Update is called once per frame
	void Update ()
    {
        Acceleration();
        Destroy();
	}

    protected virtual void Acceleration()
    {
        if (m_velocity < m_maxVelocity)
        {
            m_velocity = m_velocity + (m_accelerationRate * Time.deltaTime);
        }
    }

    protected virtual void Destroy()
    {
        if (m_flying == false && m_contact == false)
        {
            m_projectile.SetActive(false);
            m_projectileBody.velocity = Vector3.zero;
            m_projectileBody.isKinematic = true;
            m_projectileTrail.Clear();
        }
        else if (m_flying == true && m_contact == true)
        {
            Collider[] m_objectHit;

            m_objectHit = Physics.OverlapSphere(m_projectile.transform.position, m_damageRadius, m_objectMask);

            if (m_explosionEffect)
            {
 
                m_explosionEffect.gameObject.transform.position = m_projectile.transform.position;
                m_explosionEffect.SetActive(true);
            }

            if (m_explosionSound)
            {
                m_explosionSound.Play();
            }

            if (m_objectHit != null)
            {
                foreach (Collider m_object in m_objectHit)
                {
                    if (m_object.gameObject.tag == m_objectTag)
                    {
                        m_object.gameObject.GetComponent<BaseObject>().Damage(m_damage);
                        m_object.gameObject.GetComponent<BaseObject>().IsTracked = false;
                    }
                }
            }

            m_projectile.SetActive(false);
            m_projectileBody.velocity = Vector3.zero;
            m_projectileBody.isKinematic = true;
            m_projectileTrail.Clear();
        }
    }

    void OnCollisionEnter(Collision m_collision)
    {
        m_contact = true;
    }

    protected IEnumerator Thrust()
    {
        float m_time;

        m_time = 0.0f;

        while (m_time < m_flightTime)
        {
            m_time += Time.deltaTime;

            //m_projectileBody.AddRelativeForce(m_projectile.transform.forward * m_velocity);
            m_projectileBody.AddForce(m_projectile.transform.forward * m_velocity);

            yield return null;
        }

        m_time = 0.0f;
        m_flying = false;
    }
}
