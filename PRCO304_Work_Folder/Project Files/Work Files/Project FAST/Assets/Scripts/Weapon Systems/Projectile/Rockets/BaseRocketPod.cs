﻿using UnityEngine;
using System.Collections;

public class BaseRocketPod : MonoBehaviour
{
    [SerializeField]
    protected GameObject m_rocket;
    protected GameObject[] m_rocketArray;
    [SerializeField]
    protected AudioSource m_launchSound;
    [SerializeField]
    protected ParticleSystem m_launchEffect;
    protected ParticleSystem[] m_launchEffectArray;

    protected BaseFlight m_flight;

    protected GameObject m_rocketPod;
    protected Transform[] m_rocketPodArray;

    protected GameObject m_jetObject;

    protected float m_timePerRocket;
    protected float m_armTime;

    protected int m_rocketPodAmmount;

    protected bool m_rocketArmed;

    protected Collider[] m_colliderArray;

    // Use this for initialization
    void Start()
    {
        Initialization();
        RocketPool();
        EffectPool();
    }

    public virtual void Initialization()
    {
        m_rocketPod = this.gameObject;
        m_rocketPodArray = m_rocketPod.GetComponentsInChildren<Transform>();

        m_flight = m_rocketPod.GetComponentInParent<BaseFlight>();

        m_jetObject = m_rocketPod.transform.parent.gameObject;

        m_colliderArray = m_jetObject.GetComponentsInChildren<Collider>();

        m_timePerRocket = 0.2f;
        m_armTime = 11.0f;

        m_rocketPodAmmount = m_rocketPodArray.Length;
        m_rocketArmed = true;
    }

    protected virtual void RocketPool()
    {
        if (m_rocket)
        {
            m_rocketArray = new GameObject[m_rocketPodAmmount];

            for (int i = 0; i < m_rocketPodAmmount; i++)
            {
                m_rocketArray[i] = Instantiate(m_rocket) as GameObject;
                m_rocketArray[i].gameObject.SetActive(false);
            }
        }
    }

    protected virtual void EffectPool()
    {
        if (m_launchEffect)
        {
            m_launchEffectArray = new ParticleSystem[m_rocketPodAmmount];

            for (int i = 0; i < m_rocketPodAmmount; i++)
            {
                m_launchEffectArray[i] = Instantiate(m_launchEffect) as ParticleSystem;
                m_launchEffectArray[i].gameObject.SetActive(false);
            }
        }
    }

    //Update is called once per frame
    void Update()
    {

    }

    public virtual void RocketSalvo()
    {
        if (m_rocketArmed == true)
        {
            StartCoroutine(Fire());
            StartCoroutine(Arm());
        }
    }

    protected IEnumerator Fire()
    {
        float m_time;
        int m_armed;
        int m_id;

        m_time = 0.0f;
        m_armed = m_rocketPodAmmount;
        m_id = 0;

        while (m_armed > 0)
        {
            m_time += Time.deltaTime;

            if (m_time > m_timePerRocket)
            {
                if (m_rocketArray[m_id].activeSelf == false)
                {
                    m_rocketArray[m_id].SetActive(true);

                    foreach (Collider m_collider in m_colliderArray)
                    {
                        Physics.IgnoreCollision(m_rocketArray[m_id].GetComponent<Collider>(), m_collider);
                    }

                    m_rocketArray[m_id].GetComponent<BaseRocket>().Initialization(m_flight.Velocity);
                    m_rocketArray[m_id].transform.position = m_rocketPodArray[m_id].position;
                    m_rocketArray[m_id].transform.rotation = m_rocketPodArray[m_id].rotation;
                }

                //if (m_launchEffectArray[m_id].gameObject.activeSelf == false)
                //{
                //    m_launchEffectArray[m_id].gameObject.SetActive(true);
                //    m_launchEffectArray[m_id].transform.position = m_rocketPodArray[m_id].position;
                //    m_launchEffectArray[m_id].transform.rotation = m_rocketPodArray[m_id].rotation;
                //    m_launchEffectArray[m_id].Play();
                //}

                m_id += 1;
                m_armed -= 1;
                m_time = 0.0f;
            }

            yield return null;
        }

        m_rocketArmed = false;
    }

    protected IEnumerator Arm()
    {
        yield return new WaitForSeconds(m_armTime);
        m_rocketArmed = true;
    }

    public bool Armed
    {
        get
        {
            return m_rocketArmed;
        }
    }
}
