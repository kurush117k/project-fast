﻿using UnityEngine;
using System.Collections;

public class AIM120Launcher : BaseMissileLauncher
{
    public override void Initialization()
    {
        m_launchPoint = this.gameObject;
        m_launchPointArray = m_launchPoint.GetComponentsInChildren<Transform>();
        m_radar = m_launchPoint.transform.parent.gameObject;
        m_jetObject = m_radar.transform.parent.gameObject;

        m_flight = m_launchPoint.GetComponentInParent<BaseFlight>();
        m_radarLockOn = m_launchPoint.GetComponentInParent<BaseMissileLockOn>();
        m_colliderArray = m_jetObject.GetComponentsInChildren<Collider>();

        m_armTime = 15.0f;

        m_missilePayload = m_launchPointArray.Length;

        m_missileArmed = true;
    }
}
