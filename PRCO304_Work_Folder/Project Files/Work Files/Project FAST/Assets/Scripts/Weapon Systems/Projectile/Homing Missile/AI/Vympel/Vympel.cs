﻿using UnityEngine;
using System.Collections;

public class Vympel : BaseMissile
{
    public override void Initialization(float m_currentVelocity, GameObject m_radarTarget)
    {
        m_projectile = this.gameObject;
        m_projectileBody = m_projectile.GetComponent<Rigidbody>();
        m_projectileBody.isKinematic = false;

        m_projectileTrail = m_projectile.GetComponent<TrailRenderer>();

        m_target = m_radarTarget;

        m_flightTime = 5.0f;
        m_accelerationRate = 200.0f;
        m_velocity = m_currentVelocity / 100;
        m_maxVelocity = 1000.0f;
        m_damageRadius = 50.0f;
        m_targetMaxAngle = 45.0f;
        m_trajectoryDamping = 15.0f;
        m_thrustDelay = 0.5f;

        m_damage = 40;

        m_objectTag = "Player";

        m_flying = true;
        m_contact = false;

        m_objectMask = 1 << 8;

        if (m_thrustEffect)
        {
            m_thrustEffect.Play();
        }

        if (m_thrustSound)
        {
            m_thrustSound.Play();
        }

        if (m_target != null)
        {
            m_tracking = true;
            m_target.GetComponent<BaseObject>().IsTracked = true;
        }
        else if (m_target == null)
        {
            m_tracking = false;
        }

        StartCoroutine(ProjectileGravity());
        StartCoroutine(Thrust());
    }
}
