﻿using UnityEngine;
using System.Collections;

public class F15RadarLockOn : BaseMissileLockOn
{

    public override void Initialization()
    {
        m_radar = this.gameObject;

        m_radarPosition = m_radar.GetComponent<Transform>();
        m_radarRange = 2500.0f;
        m_lockonRange = 1500.0f;
        m_radarAngle = 15.0f;

        m_targetMask = 1 << 9;
    }
}
