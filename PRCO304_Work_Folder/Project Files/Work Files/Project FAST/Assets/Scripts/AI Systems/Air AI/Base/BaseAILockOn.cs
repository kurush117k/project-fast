﻿using UnityEngine;
using System.Collections;

public class BaseAILockOn : BaseMissileLockOn
{

    public override void Initialization()
    {
        m_radar = this.gameObject;

        m_radarPosition = m_radar.GetComponent<Transform>();
        m_radarRange = 2000.0f;
        m_lockonRange = 1500.0f;
        m_radarAngle = 35.0f;

        m_targetMask = 1 << 8;
    }
}
