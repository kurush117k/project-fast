﻿using UnityEngine;
using System.Collections;

public class BaseAirController : MonoBehaviour
{
    protected GameObject m_ai;
    protected BaseAirSense m_aiSense;
    protected AIFlight m_aiFlight;
    protected BaseAirAutoCannon m_aiAutoCannon;
    protected BaseMissileLauncher[] m_aiMissilelauncher;
    protected BaseObject m_aiObject;

    protected float m_pitchInputFloat;
    protected float m_rollInputFloat;
    protected float m_yawInputFloat;
    protected float m_speedInputFloat;

    protected float m_input;
    protected float m_minInput;
    protected float m_maxInput;

    protected float m_minInterceptDist;
    protected float m_maxAttackDist;
    protected float m_maxAttackAngle;

    protected float m_maxCannonDist;

    protected bool m_setRandom;

    // Use this for initialization
    void Start()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_ai = this.gameObject;
        m_aiSense = m_ai.GetComponent<BaseAirSense>();
        m_aiFlight = m_ai.GetComponent<AIFlight>();
        m_aiObject = m_ai.GetComponent<BaseObject>();
        m_aiAutoCannon = m_ai.GetComponentInChildren<BaseAirAutoCannon>();
        m_aiMissilelauncher = m_ai.GetComponentsInChildren<BaseMissileLauncher>();

        m_input = 1.0f;
        m_minInput = 0.5f;
        m_maxInput = 1.0f;

        m_minInterceptDist = 100.0f;
        m_maxAttackDist = 75.0f;
        m_maxAttackAngle = 25.0f;

        m_maxCannonDist = 25.0f;

        m_setRandom = true;
    }

    // Update is called once per frame
    void Update()
    {
        AIController();
    }

    protected virtual void AIController()
    {
        if (m_aiSense.TerrainWarning == true && (m_aiSense.XRot > 350.0f || m_aiSense.XRot < 190.0f))
        {
            TerrainAvoidance();
        }
        else if (m_aiSense.CollisionWarning == true)
        {
            CollisionAvoidance();
        }
        else if (m_aiObject.IsTracked == true)
        {
            if (m_aiSense.TerrainWarning == true && (m_aiSense.XRot > 350.0f || m_aiSense.XRot < 190.0f))
            {
                TerrainAvoidance();
            }

            m_pitchInputFloat = -m_input;
            m_rollInputFloat = 0.0f;
            m_yawInputFloat = 0.0f;

            m_speedInputFloat = m_input;

            Flight(m_pitchInputFloat, m_rollInputFloat, m_yawInputFloat, m_speedInputFloat);
        }
        else if (m_aiSense.Target != null && m_aiSense.TargetAngle <= 115.0f && m_aiObject.IsTracked == false)
        {
            m_speedInputFloat = m_maxInput;

            if (m_aiSense.TargetDistance > m_minInterceptDist)
            {
                Debug.Log("Intercept");
                m_aiFlight.Accelerate(m_speedInputFloat);
                m_aiFlight.TargetIntercept(m_aiSense.Target.transform, m_input);
            }
            else if (m_aiSense.TargetDistance <= m_minInterceptDist)
            {
                m_aiFlight.Decelerate(m_speedInputFloat);
                m_aiFlight.TargetIntercept(m_aiSense.Target.transform, m_input);

                if (m_aiSense.TargetDistance <= m_maxAttackDist && m_aiSense.TargetAngle <= m_maxAttackAngle)
                {
                    Attack();
                }
            }
        }
        else if (m_aiSense.OutOfBounds == true)
        {
            OutOfBounds();
        }
        else if (m_aiSense.TerrainWarning == false) 
        {
            if (m_aiSense.Target != null)
            {
                if (m_aiSense.TargetDistance < 250.0f)
                {
                    if (m_aiSense.TargetAngle > 115.0f)
                    {
                        if (m_setRandom == true)
                        {
                            StartCoroutine(RandomInput(m_maxInput));
                            m_setRandom = false;
                        }
                        m_speedInputFloat = 1.0f;
                        Flight(m_pitchInputFloat, m_rollInputFloat, m_yawInputFloat, m_speedInputFloat);
                    }
                    else
                    {
                        if (m_setRandom == true)
                        {
                            StartCoroutine(RandomInput(m_minInput));
                            m_setRandom = false;
                        }
                        m_speedInputFloat = 0.0f;
                        
                        Flight(m_pitchInputFloat, m_rollInputFloat, m_yawInputFloat, m_speedInputFloat);
                    }
                }
                else
                {
                    if (m_setRandom == true)
                    {
                        StartCoroutine(RandomInput(m_minInput));
                        m_setRandom = false;
                    }
                    m_speedInputFloat = 0.0f;
                    
                    Flight(m_pitchInputFloat, m_rollInputFloat, m_yawInputFloat, m_speedInputFloat);
                }
            }
            else
            {
                if (m_setRandom == true)
                {
                    StartCoroutine(RandomInput(m_minInput));
                    m_setRandom = false;
                }
                m_speedInputFloat = 0.0f;
                
                Flight(m_pitchInputFloat, m_rollInputFloat, m_yawInputFloat, m_speedInputFloat);
            }
        }
    }

    protected IEnumerator RandomInput(float m_randomInput)
    {
        m_pitchInputFloat = Random.Range(-m_randomInput, m_randomInput);
        m_rollInputFloat = Random.Range(-m_randomInput, m_randomInput);
        m_yawInputFloat = Random.Range(-m_randomInput, m_randomInput);

        yield return new WaitForSeconds(5.0f);
        m_setRandom = true;
    }

    protected virtual void Flight(float m_pitchInput, float m_rollInput, float m_yawInput, float m_speedInput)
    {
        m_aiFlight.Pitch(m_pitchInput);
        m_aiFlight.Roll(m_rollInput);
        m_aiFlight.Yaw(m_yawInput);

        if (m_speedInput == 0.0f)
        {
            m_aiFlight.DefaultSpeed();
        }
        else if (m_speedInput > 0.0f)
        {
            m_aiFlight.Accelerate(m_speedInput);
        }
        else if (m_speedInput < 0.0f)
        {
            m_aiFlight.Decelerate(-m_speedInput);
        }
    }

    protected virtual void OutOfBounds()
    {
        m_speedInputFloat = m_maxInput * 10;
        m_pitchInputFloat = -m_maxInput * 10;
        m_rollInputFloat = m_maxInput * 10;
        m_setRandom = true;

        if (m_aiSense.ZRot < 5.0f || m_aiSense.ZRot > 355.0f) 
        {
            if (m_aiSense.ZRot <= 180.0f)
            {
                m_aiFlight.Roll(-m_rollInputFloat * 1.5f);
            }
            else if (m_aiSense.ZRot > 180.0f)
            {
                m_aiFlight.Roll(m_rollInputFloat * 1.5f);
            }
        }
        else
        {
            if (m_aiSense.XRot < 350.0f && m_aiSense.XRot > 10.0f)
            {
                m_aiFlight.Pitch(m_pitchInputFloat);
            }
        }
    }

    protected virtual void CollisionAvoidance()
    {
        m_speedInputFloat = m_maxInput * 10;
        m_pitchInputFloat = -m_maxInput * 10;
        m_setRandom = true;

        m_aiFlight.Decelerate(m_speedInputFloat);
        m_aiFlight.Pitch(m_pitchInputFloat);
    }

    protected virtual void TerrainAvoidance()
    {
        m_speedInputFloat = m_maxInput * 10;
        m_pitchInputFloat = -m_maxInput * 10;
        m_rollInputFloat = m_maxInput * 10;
        m_setRandom = true;

        m_aiFlight.Decelerate(m_speedInputFloat);

        if (m_aiSense.ZRot < 5.0f && m_aiSense.ZRot < 355.0f)
        {
            m_aiFlight.Pitch(m_pitchInputFloat);
        }
        else
        {
            if (m_aiSense.ZRot <= 180.0f)
            {
                m_aiFlight.Roll(-m_rollInputFloat * 1.5f);
            }
            else if (m_aiSense.ZRot > 180.0f)
            {
                m_aiFlight.Roll(m_rollInputFloat * 1.5f);
            }
        }
    }

    protected virtual void Attack()
    {
        if (m_aiSense.TargetDistance <= m_maxCannonDist)
        {
            if (m_aiAutoCannon.TargetEngaged == true)
            {
                m_aiAutoCannon.AutocannonFire();
            }
        }
        else
        {
            foreach (BaseMissileLauncher m_launcher in m_aiMissilelauncher)
            {
                if (m_launcher.Armed == true)
                {
                    m_launcher.MissileLaunch();
                }
            }
        }
    }
}
