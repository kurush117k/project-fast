﻿using UnityEngine;
using System.Collections;

public class SU35AISense : BaseAirSense
{
    public override void Initialization()
    {
        m_ai = this.gameObject;
        m_aiTransform = m_ai.GetComponent<Transform>();

        m_maxObstacleDistance = 300.0f;
        m_obstacleWarningDist = 25.0f;
        m_obstacleWarningAngle = 15.0f;
        m_maxRadarRange = 8000;

        m_terrainMask = 1 << 29;
        m_targetMask = 1 << 8;

        m_targetTag = "Player";
    }
}
