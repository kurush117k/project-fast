﻿using UnityEngine;
using System.Collections;

public class SU35AIController : BaseAirController
{
    public override void Initialization()
    {
        m_ai = this.gameObject;
        m_aiSense = m_ai.GetComponent<BaseAirSense>();
        m_aiFlight = m_ai.GetComponent<AIFlight>();
        m_aiObject = m_ai.GetComponent<BaseObject>();
        m_aiAutoCannon = m_ai.GetComponentInChildren<BaseAirAutoCannon>();
        m_aiMissilelauncher = m_ai.GetComponentsInChildren<BaseMissileLauncher>();

        m_input = 0.05f;

        m_minInput = 0.05f;
        m_maxInput = 0.10f;

        m_minInterceptDist = 600.0f;
        m_maxAttackDist = 450.0f;
        m_maxAttackAngle = 35.0f;

        m_maxCannonDist = .0f;

        m_setRandom = true;
    }
}
