﻿using UnityEngine;
using System.Collections;

public class BaseAirAutoCannon : BaseAutocannon
{
    protected bool m_targetEngaged;

    protected Ray m_targetRay;

    public override void Initialization()
    {
        m_fireRate = 10.0f;
        m_fireTimer = 1.0f;
        m_maxRange = 1000.0f;

        m_maxAmmo = 1000;
        m_damage = 5;
        m_maxArraySize = (int)(m_maxAmmo / m_fireRate);

        m_mask = 1 << 8;

        m_targetTag = "Player";

        m_targetEngaged = false;
    }

    // Update is called once per frame
    void Update ()
    {
        Target();
    }

    protected virtual void Target()
    {
        m_targetRay = new Ray(m_muzzle.position, m_muzzle.forward);

        if (Physics.Raycast(m_targetRay, m_maxRange, m_mask))
        {
            m_targetEngaged = true;
        }
        else
        {
            m_targetEngaged = false;
        }
    }

    public bool TargetEngaged
    {
        get
        {
            return m_targetEngaged;
        }
    }
}
