﻿using UnityEngine;
using System.Collections;

public class BaseAirSense : MonoBehaviour
{
    protected GameObject m_ai;
    protected Transform m_aiTransform;

    protected GameObject m_target;
    protected string m_targetTag;

    protected float m_maxObstacleDistance;
    protected float m_obstacleWarningDist;
    protected float m_obstacleWarningAngle;
    protected float m_maxRadarRange;

    protected float m_xRot;
    protected float m_yRot;
    protected float m_zRot;

    protected float m_terrainDistance;

    protected float m_targetDistance;
    protected float m_targetAngel;

    protected LayerMask m_targetMask;
    protected LayerMask m_obstacleMask;
    protected LayerMask m_terrainMask;

    protected bool m_collisionWarning;
    protected bool m_terrainWarning;
    protected bool m_outOBounds;

    // Use this for initialization
    void Start()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_ai = this.gameObject;
        m_aiTransform = m_ai.GetComponent<Transform>();

        m_maxObstacleDistance = 300.0f;
        m_obstacleWarningDist = 50.0f;
        m_obstacleWarningAngle = 15.0f;
        m_maxRadarRange = 2500;

        m_terrainMask = 1 << 29;
        m_targetMask = 1 << 8;

        m_targetTag = "Player";
    }

    // Update is called once per frame
    void Update()
    {
        SenseObstacle();
        SenseTargets();
        SenseTerrain();
        GetOrientation();
    }

    protected void SenseObstacle()
    {
        Ray m_ray;
        RaycastHit m_hit;

        m_ray = new Ray(m_aiTransform.position, m_aiTransform.forward);
        if (Physics.SphereCast(m_ray, 7.0f, out m_hit, m_maxObstacleDistance))
        {
            m_collisionWarning = true;

            m_obstacleWarningAngle = Vector3.Angle(m_aiTransform.position, m_hit.transform.position);
        }
        else
        {
            m_collisionWarning = false;
        }
    }

    protected void SenseTargets()
    {
        Collider[] m_hitCollider;

        m_hitCollider = Physics.OverlapSphere(m_aiTransform.position, m_maxRadarRange, m_targetMask);

        foreach (Collider m_collider in m_hitCollider)
        {
            if (m_collider.tag == m_targetTag)
            {
                m_target = m_collider.gameObject;
            }
            else
            {
                m_target = null;
            }
        }

        if  (m_target != null)
        {
            Vector3 m_targetDirection;

            m_targetDirection = m_target.transform.position - m_aiTransform.position;

            m_targetAngel = Vector3.Angle(m_targetDirection, m_aiTransform.forward);
            m_targetDistance = Vector3.Distance(m_target.transform.position, m_aiTransform.position);
        }
    }

    protected void SenseTerrain()
    {
        Ray m_terrainRay;

        float m_terrainWarningThreshold;

        m_terrainRay = new Ray(m_aiTransform.position, -Vector3.up);
        m_terrainWarningThreshold = 150.0f;

        if (Physics.Raycast(m_terrainRay, m_terrainWarningThreshold, m_terrainMask))
        {
            m_terrainWarning = true;
        }
        else
        {
            m_terrainWarning = false;
        }
    }

    protected void GetOrientation()
    {
        m_xRot = m_aiTransform.rotation.eulerAngles.x;
        m_yRot = m_aiTransform.rotation.eulerAngles.y;
        m_zRot = m_aiTransform.rotation.eulerAngles.z;
    }

    public GameObject Target
    {
        get
        {
            return m_target;
        }
    }

    public float TargetDistance
    {
        get
        {
            return m_targetDistance;
        }
    }

    public float TargetAngle
    {
        get
        {
            return m_targetAngel;
        }
    }

    public float TerrainDistance
    {
        get
        {
            return m_terrainDistance;
        }
    }

    public float XRot
    {
        get
        {
            return m_xRot;
        }
    }

    public float YRot
    {
        get
        {
            return m_yRot;
        }
    }

    public float ZRot
    {
        get
        {
            return m_zRot;
        }
    }

    public bool CollisionWarning
    {
        get
        {
            return m_collisionWarning;
        }
    }

    public bool TerrainWarning
    {
        get
        {
            return m_terrainWarning;
        }
    }

    public bool OutOfBounds
    {
        get
        {
            return m_outOBounds;
        }
        set
        {
            m_outOBounds = value;
        }
    }
}
