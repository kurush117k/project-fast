﻿using UnityEngine;
using System.Collections;

public class BaseTarget : MonoBehaviour
{
    protected GameObject m_radar;
    protected Transform m_radarPosition;
    protected GameObject m_target;
    protected LayerMask m_mask;

    protected float m_radarRange;
    protected float m_engagementRange;
    protected float m_engagementAngle;

    protected string m_targetTag;

    protected bool m_targetEngaged;

    // Use this for initialization
    void Start()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_radar = this.gameObject;
        m_radarPosition = m_radar.GetComponent<Transform>();

        m_mask = 1 << 8;

        m_radarRange = 150.0f;
        m_engagementRange = 100.0f;
        m_engagementAngle = 180.0f;

        m_targetTag = "";

        m_targetEngaged = false;
    }

    // Update is called once per frame
    void Update()
    {
        DetectTarget();
        EngageTarget();
    }

    protected virtual void DetectTarget()
    {
        Collider[] m_targetInRange;

        m_targetInRange = Physics.OverlapSphere(m_radarPosition.position, m_radarRange, m_mask);

        if (m_targetInRange.Length > 0)
        {
            foreach (Collider m_collider in m_targetInRange)
            {
                if (m_collider.tag == m_targetTag)
                {
                    m_target = m_collider.gameObject;
                }
            }
        }
        else
        {
            m_target = null;
        }
    }

    protected virtual void EngageTarget()
    {
        if (m_target != null)
        {
            Vector3 m_targetDirection;
            float m_targetAngle;
            float m_targetRange;

            m_targetDirection = m_target.transform.position - m_radarPosition.position;

            m_targetAngle = Vector3.Angle(m_targetDirection, m_radarPosition.up);
            m_targetRange = Vector3.Distance(m_target.transform.position, m_radarPosition.position);

            if (m_targetRange <= m_engagementRange)
            {
                if (m_targetAngle <= m_engagementAngle)
                {
                    m_target.GetComponent<BaseObject>().IsLockedOn = true;
                    m_targetEngaged = true;
                }
                else
                {
                    m_target.GetComponent<BaseObject>().IsLockedOn = false;
                    m_targetEngaged = false;
                }
            }
            else
            {
                m_target.GetComponent<BaseObject>().IsLockedOn = false;
                m_targetEngaged = false;
            }
        }
        else
        {
            m_targetEngaged = false;
        }
    }

    public bool TargetEngaged
    {
        get
        {
            return m_targetEngaged;
        }
    }
}
