﻿using UnityEngine;
using System.Collections;

public class SAMController : BaseGroundController
{
    protected SAMLauncher m_samLauncher;

    public override void Initialization()
    {
        m_target = GetComponentInChildren<BaseTarget>();
        m_samLauncher = GetComponentInChildren<SAMLauncher>();
    }

    protected override void Fire()
    {
        if (m_target.TargetEngaged == true)
        {
            if (m_samLauncher.Armed == true)
            {
                m_samLauncher.MissileLaunch();
            }
        }
    }
}
