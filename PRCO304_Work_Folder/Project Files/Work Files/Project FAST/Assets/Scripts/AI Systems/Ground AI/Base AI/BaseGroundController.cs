﻿using UnityEngine;
using System.Collections;

public class BaseGroundController : MonoBehaviour
{
    protected BaseTarget m_target;

    // Use this for initialization
    void Start()
    {
        Initialization();
    }

    public virtual void Initialization()
    {
        m_target = GetComponentInChildren<BaseTarget>();
    }

    // Update is called once per frame
    void Update()
    {
        Fire();
    }

    protected virtual void Fire()
    {

    }
}
