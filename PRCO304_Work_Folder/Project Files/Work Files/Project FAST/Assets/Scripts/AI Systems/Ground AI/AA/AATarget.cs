﻿using UnityEngine;
using System.Collections;

public class AATarget : BaseTarget
{
    public override void Initialization()
    {
        m_radar = this.gameObject;
        m_radarPosition = m_radar.GetComponent<Transform>();

        m_mask = 1 << 8;

        m_radarRange = 500.0f;
        m_engagementRange = 350.0f;
        m_engagementAngle = 180.0f;

        m_targetTag = "Player";

        m_targetEngaged = false;
    }
}
