﻿using UnityEngine;
using System.Collections;

public class SAMTarget : BaseTarget
{
    [SerializeField]
    protected Transform m_launchPoint;

    public override void Initialization()
    {
        m_radar = this.gameObject;
        m_radarPosition = m_radar.GetComponent<Transform>();

        m_mask = 1 << 8;

        m_radarRange = 1000.0f;
        m_engagementRange = 750.0f;
        m_engagementAngle = 180.0f;

        m_targetTag = "Player";

        m_targetEngaged = false;
    }

    void Update()
    {
        DetectTarget();
        EngageTarget();

        if (m_targetEngaged == true)
        {
            LookAtTarget();
        }
    }

    protected virtual void LookAtTarget()
    {
        Quaternion m_targetLook;

        m_targetLook = Quaternion.LookRotation(m_target.transform.position, m_launchPoint.position);
        //m_launchPoint.rotation = Quaternion.Slerp(m_launchPoint.transform.rotation, m_targetLook, Time.deltaTime * 10.0f);
        m_launchPoint.rotation = m_targetLook;
    }

    public GameObject Target
    {
        get
        {
            return m_target;
        }
    }
}
