﻿using UnityEngine;
using System.Collections;

public class AAController : BaseGroundController
{
    protected AACannon m_aaCannon;

    public override void Initialization()
    {
        m_target = GetComponentInChildren<BaseTarget>();
        m_aaCannon = GetComponentInChildren<AACannon>();
    }

    protected override void Fire()
    {
        if (m_target.TargetEngaged == true)
        {
            m_aaCannon.AutocannonFire();
        }
    }
}
