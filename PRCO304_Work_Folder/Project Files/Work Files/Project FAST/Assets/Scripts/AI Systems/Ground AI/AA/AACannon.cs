﻿using UnityEngine;
using System.Collections;

public class AACannon : BaseAutocannon
{
    public override void Initialization()
    {
        m_fireRate = 10.0f;
        m_fireTimer = 1.0f;
        m_maxRange = 1000.0f;

        m_maxAmmo = 1000;
        m_damage = 5;
        m_maxArraySize = (int)(m_maxAmmo / m_fireRate);

        m_mask = 1 << 8;

        m_targetTag = "Player";
    }
}
